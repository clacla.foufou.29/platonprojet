<?php

namespace App\Entity;

use App\Repository\DepartementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ApiResource()
 * @ORM\Entity(repositoryClass=DepartementRepository::class)
 */
class Departement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomDepartement;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="DepartementDeResidence")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="departementDeNaissance")
     */
    private $habitants;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->habitants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getNomDepartement(): ?string
    {
        return $this->nomDepartement;
    }

    public function setNomDepartement(string $nomDepartement): self
    {
        $this->nomDepartement = $nomDepartement;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setDepartementDeResidence($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getDepartementDeResidence() === $this) {
                $user->setDepartementDeResidence(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getNomDepartement();
    }

    /**
     * @return Collection|User[]
     */
    public function getHabitants(): Collection
    {
        return $this->habitants;
    }

    public function addHabitant(User $habitant): self
    {
        if (!$this->habitants->contains($habitant)) {
            $this->habitants[] = $habitant;
            $habitant->setDepartementDeNaissance($this);
        }

        return $this;
    }

    public function removeHabitant(User $habitant): self
    {
        if ($this->habitants->removeElement($habitant)) {
            // set the owning side to null (unless already changed)
            if ($habitant->getDepartementDeNaissance() === $this) {
                $habitant->setDepartementDeNaissance(null);
            }
        }

        return $this;
    }
}
