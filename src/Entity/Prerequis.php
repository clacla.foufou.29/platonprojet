<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PrerequisRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=PrerequisRepository::class)
 */
class Prerequis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libellePrerequis;

    
    public function __construct()
    {
        $this->formations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibellePrerequis(): ?string
    {
        return $this->libellePrerequis;
    }

    public function setLibellePrerequis(string $libellePrerequis): self
    {
        $this->libellePrerequis = $libellePrerequis;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getLibellePrerequis();
    }
}
