<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CoursStagiairesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoursStagiairesRepository::class)
 */
#[ApiResource]
class CoursStagiaires
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $apte;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $present;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $motif;

    /**
     * @ORM\ManyToOne(targetEntity=Cours::class, inversedBy="coursStagiaires")
     */
    private $cours;

    /**
     * @ORM\ManyToOne(targetEntity=Stagiaire::class, inversedBy="coursStagiaires")
     */
    private $stagiaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApte(): ?bool
    {
        return $this->apte;
    }

    public function setApte(?bool $apte): self
    {
        $this->apte = $apte;

        return $this;
    }

    public function getPresent(): ?bool
    {
        return $this->present;
    }

    public function setPresent(?bool $present): self
    {
        $this->present = $present;

        return $this;
    }

    public function getMotif(): ?string
    {
        return $this->motif;
    }

    public function setMotif(string $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    public function getStagiaire(): ?Stagiaire
    {
        return $this->stagiaire;
    }

    public function setStagiaire(?Stagiaire $stagiaire): self
    {
        $this->stagiaire = $stagiaire;

        return $this;
    }
}
