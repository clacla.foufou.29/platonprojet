<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UploadImageDiplomeRepository;

/**
 * ApiResource()
 * @ORM\Entity(repositoryClass=UploadImageDiplomeRepository::class)
 */
class UploadImageDiplome
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToOne(targetEntity=Diplome::class, mappedBy="uploadImageDiplome", cascade={"persist", "remove"})
     */
    private $diplome;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDiplome(): ?Diplome
    {
        return $this->diplome;
    }

    public function setDiplome(?Diplome $diplome): self
    {
        // unset the owning side of the relation if necessary
        if ($diplome === null && $this->diplome !== null) {
            $this->diplome->setUploadImageDiplome(null);
        }

        // set the owning side of the relation if necessary
        if ($diplome !== null && $diplome->getUploadImageDiplome() !== $this) {
            $diplome->setUploadImageDiplome($this);
        }

        $this->diplome = $diplome;

        return $this;
    }


}
