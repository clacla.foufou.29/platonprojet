<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CotisationRepository;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * ApiResource()
 * @ORM\Entity(repositoryClass=CotisationRepository::class)
 */
class Cotisation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelleCotisation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeCotisation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $anneeCotisation;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixCotisation;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="cotisations")
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $MoyenDePaiement;


    private $vads_amount;
    private $vads_action_mode;
    private $vads_capture_delay;
    private $ctx_mode;
    private $vads_currency;
    private $vads_page_action;
    private $vads_payment_cards;
    private $vads_payment_config;
    private $vads_site_id;
    private $vads_trans_date;
    private $vads_trans_id;
    private $vads_version;
    private $signature;
    private $certificate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleCotisation(): ?string
    {
        return $this->libelleCotisation;
    }

    public function setLibelleCotisation(string $libelleCotisation): self
    {
        $this->libelleCotisation = $libelleCotisation;

        return $this;
    }

    public function getTypeCotisation(): ?string
    {
        return $this->typeCotisation;
    }

    public function setTypeCotisation(string $typeCotisation): self
    {
        $this->typeCotisation = $typeCotisation;

        return $this;
    }

    public function getAnneeCotisation(): ?string
    {
        return $this->anneeCotisation;
    }

    public function setAnneeCotisation(string $anneeCotisation): self
    {
        $this->anneeCotisation = $anneeCotisation;

        return $this;
    }

    public function getPrixCotisation(): ?int
    {
        return $this->prixCotisation;
    }

    public function setPrixCotisation(int $prixCotisation): self
    {
        $this->prixCotisation = $prixCotisation;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->users;
    }

    public function setUser(?User $user): self
    {
        $this->users = $user;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getLibelleCotisation();
    }

    public function getMoyenDePaiement(): ?string
    {
        return $this->MoyenDePaiement;
    }

    public function setMoyenDePaiement(string $MoyenDePaiement): self
    {
        $this->MoyenDePaiement = $MoyenDePaiement;

        return $this;
    }

    public function getVadsAmount(): ?string
    {
        return $this->getPrixCotisation();
    }

    public function getVadsActionMode(): ?string
    {
        return "INTERACTIVE";
    }
    public function getVadsCaptureDelay(): ?string
    {
        return "0";
    }

    public function getCtxMode(): ?string
    {
        return "TEST";
    }

    public function getVadsCurrency(): ?string
    {
        return "978";
    }

    public function getVadsPageAction(): ?string
    {
        return "PAYMENT";
    }

    public function getVadsPaymentCards(): ?string
    {
        return "CB";
    }

    public function getVadsPaymentConfig(): ?string
    {
        return "SINGLE";
    }

    public function getVadsSiteId(): ?string
    {
        return "20169455";
    }

    public function getVadsTransDate(): ?string
    {
        return gmdate('YmdHis');
    }

    public function getVadsTransId(): ?string
    {
        return substr(time(), -6);
    }

    public function getVadsVersion(): ?string
    {
        return "V2";
    }

    public function getCertificate(): ?string
    {
        return "rQ1dYFoUrLsTCfdr";
    }

    public function getSignature(): ?string
    {
        $signature_content = $this->getVadsActionMode().'+'.
        strval($this->getVadsAmount()*100).'+'.
        //$this->getVadsCaptureDelay().'+'.
        $this->getCtxMode().'+'.
        strval($this->getVadsCurrency()).'+'.
        $this->getVadsPageAction().'+'.
        $this->getVadsPaymentConfig().'+'.
        strval($this->getVadsSiteId()).'+'.
        strval($this->getVadsTransDate()).'+'.
        strval($this->getVadsTransId()).'+'.
        $this->getVadsVersion();
        $signature_content .= '+'.$this->getCertificate();
        $signature = base64_encode(hash_hmac('sha256',$signature_content, $this->getCertificate(), true));
        return $signature;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }

}
