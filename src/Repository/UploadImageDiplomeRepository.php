<?php

namespace App\Repository;

use App\Entity\UploadImageDiplome;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UploadImageDiplome|null find($id, $lockMode = null, $lockVersion = null)
 * @method UploadImageDiplome|null findOneBy(array $criteria, array $orderBy = null)
 * @method UploadImageDiplome[]    findAll()
 * @method UploadImageDiplome[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UploadImageDiplomeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UploadImageDiplome::class);
    }

    // /**
    //  * @return UploadImageDiplome[] Returns an array of UploadImageDiplome objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UploadImageDiplome
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
