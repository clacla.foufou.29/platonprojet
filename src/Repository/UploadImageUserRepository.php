<?php

namespace App\Repository;

use App\Entity\UploadImageUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UploadImageUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method UploadImageUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method UploadImageUser[]    findAll()
 * @method UploadImageUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UploadImageUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UploadImageUser::class);
    }

    // /**
    //  * @return UploadImageUser[] Returns an array of UploadImageUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UploadImageUser
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
