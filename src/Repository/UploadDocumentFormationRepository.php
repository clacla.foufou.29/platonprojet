<?php

namespace App\Repository;

use App\Entity\UploadDocumentFormation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UploadDocumentFormation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UploadDocumentFormation|null findOneBy(array $criteria, array $orderBy = null)
 * @method UploadDocumentFormation[]    findAll()
 * @method UploadDocumentFormation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UploadDocumentFormationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UploadDocumentFormation::class);
    }

    // /**
    //  * @return UploadDocumentFormation[] Returns an array of UploadDocumentFormation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UploadDocumentFormation
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
