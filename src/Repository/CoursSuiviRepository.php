<?php

namespace App\Repository;

use App\Entity\CoursSuivi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CoursSuivi|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoursSuivi|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoursSuivi[]    findAll()
 * @method CoursSuivi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursSuiviRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoursSuivi::class);
    }

    // /**
    //  * @return CoursSuivi[] Returns an array of CoursSuivi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CoursSuivi
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
