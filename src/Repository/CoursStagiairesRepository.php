<?php

namespace App\Repository;

use App\Entity\CoursStagiaires;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CoursStagiaires|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoursStagiaires|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoursStagiaires[]    findAll()
 * @method CoursStagiaires[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursStagiairesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoursStagiaires::class);
    }

    // /**
    //  * @return CoursStagiaires[] Returns an array of CoursStagiaires objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CoursStagiaires
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
