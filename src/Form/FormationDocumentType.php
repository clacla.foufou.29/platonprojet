<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Categorie;
use App\Entity\Formation;
use App\Entity\Prerequis;
use App\Entity\Prerogative;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class FormationDocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('libelleFormation', TextType::class,  [
            'disabled' => true ,
        ])
        ->add('age', IntegerType::class,  [
            'disabled' => true ,
        ])
        ->add('nbMinStagiaire', IntegerType::class,  [
            'disabled' => true ,
        ])
        ->add('nbMaxStagiaire', IntegerType::class,  [
            'disabled' => true ,
        ])
        ->add('nbFormateur', IntegerType::class,  [
            'disabled' => true ,
        ])
        ->add('prixFormation', IntegerType::class,  [
            'disabled' => true ,
        ])
        ->add('categorie', EntityType::class, [
            'disabled' => true,
            'class' => Categorie::class,
            'choice_label' => function(Categorie $categorie) {
                return sprintf('%s', $categorie->getNomCategorie());
            },
            'placeholder' => 'Choisir une catégorie'
        ])
        ->add('cours', EntityType::class, [
            'disabled' => true,
            'class' => Cours::class,
            'multiple' => true,
            'choice_label' => function(Cours $cours) {
                return sprintf('%s', $cours->getNomCours());
            },
            'placeholder' => 'Choisir un cours'
        ])
        ->add('prerogatives', EntityType::class, [
            'disabled' => true,
            'class' => Prerogative::class,
            'multiple' => true,
            'placeholder' => 'Choisir une ou des prerogatives'
        ],)
        ->add('prerequis', EntityType::class, [
            'disabled' => true,
            'class' => Prerequis::class,
            'multiple' => true,
            'placeholder' => 'Choisir un ou des prerequis'
        ],)
        ->add('uploadDocumentFormation' , FileType::class,[
            'label' => false,
            'multiple' => true,
            'mapped' => false,
            'required' => false
        ],)
        ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formation::class,
        ]);
    }
}
