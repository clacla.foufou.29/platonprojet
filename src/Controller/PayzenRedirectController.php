<?php

namespace App\Controller;


use DateTime;
use Monolog\Logger;
use App\Entity\Invoince;
use App\Entity\Cotisation;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Lyra\PayzenBundle\Payzen\PayzenService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Lyra\PayzenBundle\Includes\PayzenParamsUrl;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PayzenRedirectController extends AbstractController

{
        /**
         * @Route("/payzenRedirectForm/number3")
         */
        public function number()
        {
            $number = random_int(0, 100);

            return $this->render('payzenRedirectForm/number.html.twig', [
                'number' => $number,
            ]);
        }


        /**
         * @Route("/payzenRedirectForm/redirectPaymentLyraExample")
         */
        public function redirect_payment_lyra (PayzenService $payzen,ContainerInterface $container, Request $request)
        {

            

            $params= []; 

            $array_keys = $request->query->keys();
            foreach ($array_keys as &$key) {
                $params [$key] = $request->query->get($key);
            }

            $Form = $payzen->setRequestParams($params,$container);
        

            return $this->render ('payzenRedirectForm/formPayment.html.twig', [
                'payzen' => $Form
            ]);
        }
        
        

         /**
         * @Route("/payzenRedirectForm/renderForm")
         */
        public function create_form (PayzenService $payzen,ContainerInterface $container, Request $request)
        {

            $payzenParamsUrl = new PayzenParamsUrl();
            $payzenParamsUrl->setAmount("1000000");
            $payzenParamsUrl->setCaptureDelay("7");
            $payzenParamsUrl->setCurrency("170");

            $payzenParamsUrl->setOrderId("820f1e8b7bde43fd95e18e3eac6e5644");
            $payzenParamsUrl->setReturnMode("GET");
            $payzenParamsUrl->setPageAction("PAYMENT");

            $form = $this->createFormBuilder($payzenParamsUrl)
            ->add('amount', TextType::class)
            ->add('capture_delay', HiddenType::class)
            ->add('currency', HiddenType::class)
            ->add('order_id', HiddenType::class)
            ->add('return_mode', HiddenType::class)
            ->add('page_action', HiddenType::class)
            ->add('Proced_checkout', SubmitType::class, ['label' => 'Checkout'])
            ->setAction($this->generateUrl('payment_lyra'))
            ->getForm();

            return $this->render('payzenRedirectForm/render.form.post.twig', [
                'form' => $form->createView(),
            ]);

        }





        /**
     * @Route("/invoice", name="create_invoice")
     */
    public function createInvoice(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = new Cotisation();
        $invoice->setId($request->query->get("id"));
        $invoice->setTotalAmount($request->query->get("prixCotisation"));
        $entityManager->persist($invoice);
        $entityManager->flush();
        return new Response('Saved new invoice with id ');
    }

}
