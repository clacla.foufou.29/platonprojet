<?php

namespace App\Controller;

use Twig\Environment;
use App\Entity\Diplome;
use App\Form\DiplomeType;
use App\Entity\UploadImageDiplome;
use App\Repository\DiplomeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class DiplomeController extends AbstractController
{
    #[Route('/diplome', name: 'diplome')]
    public function index(DiplomeRepository $diplome): Response
    {
        return $this->render('diplome/index.html.twig', [
            'diplomes' => $diplome->findAll(),
        ]);
    }

    #[Route('/diplome/ajout', name:'diplome_ajout')]
    public function ajoutDiplome(Request $request, SluggerInterface $slugger)
    {
        $diplome = new Diplome;

        $form = $this->createForm(DiplomeType::class, $diplome);
        $diplome->setAccept(false);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ){
            // On récupère les images transmises
            if($form->get('uploadImageDiplome')->getData()!= null){
                $image = $form->get('uploadImageDiplome')->getData();
                $extension = $image->guessExtension();
                $fichier = md5(uniqid()).'.'.$extension;
                    
                // On copie le fichier dans le dossier uploads
                $image->move($this->getParameter('diplome_images'),$fichier);
                // On crée l'image dans la base de données
                $img = new UploadImageDiplome();
                $img->setNom($fichier);
                $diplome->setUploadImageDiplome($img);
            }
            $diplome->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($diplome);
            $em->flush();
            return $this->redirectToRoute('diplome');
        }

        return $this->render('diplome/ajout.html.twig', [
            'diplome_form' => $form->createView()
        ]);
    }

    #[Route('/diplome/{id}', name: 'diplome_show')]
    public function show(Environment $twig, Diplome $diplome, DiplomeRepository $diplomeRepository): Response
    {
        return new Response($twig->render('diplome/show.html.twig', [
            'diplome' => $diplome,
        ]));
    }

    #[Route('/diplome/edit/{id}', name:'diplome_edit')]
    public function editDiplome(Diplome $diplome, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(DiplomeType::class, $diplome);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $image = $form->get('uploadImageDiplome')->getData();
            $extension = $image->guessExtension();
            $fichier = md5(uniqid()).'.'.$extension;
                
            // On copie le fichier dans le dossier uploads
            $image->move($this->getParameter('diplome_images'),$fichier);
            // On crée l'image dans la base de données
            $img = new UploadImageDiplome();
            $img->setNom($fichier);
            $diplome->setUploadImageDiplome($img);
            $diplome->setAccept(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($diplome);
            $em->flush();

            return $this->redirectToRoute('diplome');
        }

        return $this->render('diplome/edit.html.twig', [
            'diplome_form' => $form->createView(),
            'diplome' => $diplome
        ]);
    }

    #[Route('/diplome/supprimer/{id}', name:'diplome_supprimer')]
    public function supprimerDiplome(Diplome $diplome)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($diplome);
        $em->flush();

        $this->addFlash('message', 'Diplome supprimé avec succès');
        return $this->redirectToRoute('diplome');
    }

}
