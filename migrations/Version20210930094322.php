<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210930094322 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD departement_de_residence_id INT DEFAULT NULL, DROP departement_de_residence');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6492601B02A FOREIGN KEY (departement_de_residence_id) REFERENCES departement (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6492601B02A ON user (departement_de_residence_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6492601B02A');
        $this->addSql('DROP INDEX IDX_8D93D6492601B02A ON user');
        $this->addSql('ALTER TABLE user ADD departement_de_residence VARCHAR(500) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP departement_de_residence_id');
    }
}
