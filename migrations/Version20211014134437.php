<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211014134437 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE formation_prerequis');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE formation_prerequis (formation_id INT NOT NULL, prerequis_id INT NOT NULL, INDEX IDX_9EBBD45200282E (formation_id), INDEX IDX_9EBBD4ED196790 (prerequis_id), PRIMARY KEY(formation_id, prerequis_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE formation_prerequis ADD CONSTRAINT FK_9EBBD45200282E FOREIGN KEY (formation_id) REFERENCES formation (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formation_prerequis ADD CONSTRAINT FK_9EBBD4ED196790 FOREIGN KEY (prerequis_id) REFERENCES prerequis (id) ON UPDATE NO ACTION ON DELETE CASCADE');
    }
}
