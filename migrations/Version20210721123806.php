<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210721123806 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cours_stagiaires (id INT AUTO_INCREMENT NOT NULL, cours_id INT DEFAULT NULL, stagiaire_id INT DEFAULT NULL, apte TINYINT(1) DEFAULT NULL, present TINYINT(1) DEFAULT NULL, motif VARCHAR(255) DEFAULT NULL, INDEX IDX_4F675FEF7ECF78B0 (cours_id), INDEX IDX_4F675FEFBBA93DD6 (stagiaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cours_stagiaires ADD CONSTRAINT FK_4F675FEF7ECF78B0 FOREIGN KEY (cours_id) REFERENCES cours (id)');
        $this->addSql('ALTER TABLE cours_stagiaires ADD CONSTRAINT FK_4F675FEFBBA93DD6 FOREIGN KEY (stagiaire_id) REFERENCES stagiaire (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE cours_stagiaires');
    }
}
