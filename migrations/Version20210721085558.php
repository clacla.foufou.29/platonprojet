<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210721085558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, nom_categorie VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cotisation (id INT AUTO_INCREMENT NOT NULL, users_id INT DEFAULT NULL, libelle_cotisation VARCHAR(255) NOT NULL, type_cotisation VARCHAR(255) NOT NULL, annee_cotisation VARCHAR(255) NOT NULL, prix_cotisation INT NOT NULL, moyen_de_paiement VARCHAR(255) NOT NULL, INDEX IDX_AE64D2ED67B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cours (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, delegation_id INT DEFAULT NULL, formateur_principal_id INT DEFAULT NULL, structure_id INT DEFAULT NULL, nom_cours VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, cp INT NOT NULL, ville VARCHAR(255) NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, nb_stagiaire INT NOT NULL, validation_cours TINYINT(1) NOT NULL, etat VARCHAR(255) NOT NULL, INDEX IDX_FDCA8C9C5200282E (formation_id), INDEX IDX_FDCA8C9C56CBBCF5 (delegation_id), INDEX IDX_FDCA8C9C9E1E1A42 (formateur_principal_id), INDEX IDX_FDCA8C9C2534008B (structure_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cours_suivi (id INT AUTO_INCREMENT NOT NULL, cours_id INT NOT NULL, stagiaire_id INT DEFAULT NULL, user_id INT DEFAULT NULL, apte TINYINT(1) DEFAULT NULL, present TINYINT(1) DEFAULT NULL, motif VARCHAR(255) DEFAULT NULL, INDEX IDX_1EDA43597ECF78B0 (cours_id), INDEX IDX_1EDA4359BBA93DD6 (stagiaire_id), INDEX IDX_1EDA4359A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delegation (id INT AUTO_INCREMENT NOT NULL, nom_delegation VARCHAR(255) NOT NULL, delegue INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diplome (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, upload_image_diplome_id INT DEFAULT NULL, formation_id INT DEFAULT NULL, libelle_formation VARCHAR(255) NOT NULL, date_debut_formation DATE NOT NULL, date_fin_formation DATE NOT NULL, montant_diplome INT NOT NULL, accept TINYINT(1) NOT NULL, INDEX IDX_EB4C4D4EA76ED395 (user_id), UNIQUE INDEX UNIQ_EB4C4D4E79482D95 (upload_image_diplome_id), INDEX IDX_EB4C4D4E5200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, nom_document VARCHAR(255) NOT NULL, categorie VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formation (id INT AUTO_INCREMENT NOT NULL, categorie_id INT DEFAULT NULL, user_id INT DEFAULT NULL, libelle_formation VARCHAR(255) NOT NULL, age INT NOT NULL, nb_min_stagiaire INT NOT NULL, nb_max_stagiaire INT NOT NULL, nb_formateur INT NOT NULL, prix_formation INT NOT NULL, INDEX IDX_404021BFBCF5E72D (categorie_id), INDEX IDX_404021BFA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formation_prerequis (formation_id INT NOT NULL, prerequis_id INT NOT NULL, INDEX IDX_9EBBD45200282E (formation_id), INDEX IDX_9EBBD4ED196790 (prerequis_id), PRIMARY KEY(formation_id, prerequis_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, sender_id INT DEFAULT NULL, recipient_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, message VARCHAR(255) NOT NULL, date_creation DATETIME NOT NULL, vue TINYINT(1) NOT NULL, INDEX IDX_BF5476CAF624B39D (sender_id), INDEX IDX_BF5476CAE92F8F78 (recipient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment (id INT AUTO_INCREMENT NOT NULL, number VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, client_email VARCHAR(255) DEFAULT NULL, client_id VARCHAR(255) DEFAULT NULL, total_amount INT DEFAULT NULL, currency_code VARCHAR(255) DEFAULT NULL, details JSON NOT NULL COMMENT \'(DC2Type:json_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payment_token (hash VARCHAR(255) NOT NULL, details LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\', after_url LONGTEXT DEFAULT NULL, target_url LONGTEXT NOT NULL, gateway_name VARCHAR(255) NOT NULL, PRIMARY KEY(hash)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prerequis (id INT AUTO_INCREMENT NOT NULL, libelle_prerequis VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prerogative (id INT AUTO_INCREMENT NOT NULL, libelle_prerogative VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prerogative_formation (prerogative_id INT NOT NULL, formation_id INT NOT NULL, INDEX IDX_162EFC727DA4404 (prerogative_id), INDEX IDX_162EFC75200282E (formation_id), PRIMARY KEY(prerogative_id, formation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE scheduled_command (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, command VARCHAR(100) NOT NULL, arguments LONGTEXT DEFAULT NULL, cron_expression VARCHAR(100) DEFAULT NULL, last_execution DATETIME NOT NULL, last_return_code INT DEFAULT NULL, log_file VARCHAR(100) DEFAULT NULL, priority INT NOT NULL, execute_immediately TINYINT(1) NOT NULL, disabled TINYINT(1) NOT NULL, locked TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stagiaire (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_de_naissance DATE NOT NULL, ville_de_naissance VARCHAR(255) NOT NULL, lieu_de_naissance VARCHAR(255) NOT NULL, contact VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE structure (id INT AUTO_INCREMENT NOT NULL, nom_structure VARCHAR(255) NOT NULL, adresse_structure VARCHAR(255) NOT NULL, cp_structure VARCHAR(255) NOT NULL, ville_structure VARCHAR(255) NOT NULL, raison_sociale VARCHAR(255) NOT NULL, mail_structure VARCHAR(255) NOT NULL, telephone_structure VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload_document_formation (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_6FA174D65200282E (formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload_image_diplome (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload_image_user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, delegation_id INT DEFAULT NULL, upload_image_user_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, date_naissance DATE NOT NULL, num_adh VARCHAR(255) NOT NULL, lieu_de_naissance VARCHAR(500) DEFAULT NULL, pays_de_residence VARCHAR(500) DEFAULT NULL, departement_de_residence VARCHAR(500) DEFAULT NULL, pays_de_naissance VARCHAR(500) DEFAULT NULL, adresse_physique VARCHAR(500) DEFAULT NULL, ville VARCHAR(500) DEFAULT NULL, code_postale VARCHAR(255) DEFAULT NULL, profession VARCHAR(500) DEFAULT NULL, departement_de_naissance VARCHAR(500) DEFAULT NULL, tarification VARCHAR(500) DEFAULT NULL, is_active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649E42B3E62 (num_adh), INDEX IDX_8D93D64956CBBCF5 (delegation_id), UNIQUE INDEX UNIQ_8D93D649CEF8E52B (upload_image_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_cours (user_id INT NOT NULL, cours_id INT NOT NULL, INDEX IDX_1F0877C4A76ED395 (user_id), INDEX IDX_1F0877C47ECF78B0 (cours_id), PRIMARY KEY(user_id, cours_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cotisation ADD CONSTRAINT FK_AE64D2ED67B3B43D FOREIGN KEY (users_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cours ADD CONSTRAINT FK_FDCA8C9C5200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE cours ADD CONSTRAINT FK_FDCA8C9C56CBBCF5 FOREIGN KEY (delegation_id) REFERENCES delegation (id)');
        $this->addSql('ALTER TABLE cours ADD CONSTRAINT FK_FDCA8C9C9E1E1A42 FOREIGN KEY (formateur_principal_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cours ADD CONSTRAINT FK_FDCA8C9C2534008B FOREIGN KEY (structure_id) REFERENCES structure (id)');
        $this->addSql('ALTER TABLE cours_suivi ADD CONSTRAINT FK_1EDA43597ECF78B0 FOREIGN KEY (cours_id) REFERENCES cours (id)');
        $this->addSql('ALTER TABLE cours_suivi ADD CONSTRAINT FK_1EDA4359BBA93DD6 FOREIGN KEY (stagiaire_id) REFERENCES stagiaire (id)');
        $this->addSql('ALTER TABLE cours_suivi ADD CONSTRAINT FK_1EDA4359A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE diplome ADD CONSTRAINT FK_EB4C4D4EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE diplome ADD CONSTRAINT FK_EB4C4D4E79482D95 FOREIGN KEY (upload_image_diplome_id) REFERENCES upload_image_diplome (id)');
        $this->addSql('ALTER TABLE diplome ADD CONSTRAINT FK_EB4C4D4E5200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE formation ADD CONSTRAINT FK_404021BFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE formation_prerequis ADD CONSTRAINT FK_9EBBD45200282E FOREIGN KEY (formation_id) REFERENCES formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formation_prerequis ADD CONSTRAINT FK_9EBBD4ED196790 FOREIGN KEY (prerequis_id) REFERENCES prerequis (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAF624B39D FOREIGN KEY (sender_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAE92F8F78 FOREIGN KEY (recipient_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE prerogative_formation ADD CONSTRAINT FK_162EFC727DA4404 FOREIGN KEY (prerogative_id) REFERENCES prerogative (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prerogative_formation ADD CONSTRAINT FK_162EFC75200282E FOREIGN KEY (formation_id) REFERENCES formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE upload_document_formation ADD CONSTRAINT FK_6FA174D65200282E FOREIGN KEY (formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64956CBBCF5 FOREIGN KEY (delegation_id) REFERENCES delegation (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649CEF8E52B FOREIGN KEY (upload_image_user_id) REFERENCES upload_image_user (id)');
        $this->addSql('ALTER TABLE user_cours ADD CONSTRAINT FK_1F0877C4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_cours ADD CONSTRAINT FK_1F0877C47ECF78B0 FOREIGN KEY (cours_id) REFERENCES cours (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BFBCF5E72D');
        $this->addSql('ALTER TABLE cours_suivi DROP FOREIGN KEY FK_1EDA43597ECF78B0');
        $this->addSql('ALTER TABLE user_cours DROP FOREIGN KEY FK_1F0877C47ECF78B0');
        $this->addSql('ALTER TABLE cours DROP FOREIGN KEY FK_FDCA8C9C56CBBCF5');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64956CBBCF5');
        $this->addSql('ALTER TABLE cours DROP FOREIGN KEY FK_FDCA8C9C5200282E');
        $this->addSql('ALTER TABLE diplome DROP FOREIGN KEY FK_EB4C4D4E5200282E');
        $this->addSql('ALTER TABLE formation_prerequis DROP FOREIGN KEY FK_9EBBD45200282E');
        $this->addSql('ALTER TABLE prerogative_formation DROP FOREIGN KEY FK_162EFC75200282E');
        $this->addSql('ALTER TABLE upload_document_formation DROP FOREIGN KEY FK_6FA174D65200282E');
        $this->addSql('ALTER TABLE formation_prerequis DROP FOREIGN KEY FK_9EBBD4ED196790');
        $this->addSql('ALTER TABLE prerogative_formation DROP FOREIGN KEY FK_162EFC727DA4404');
        $this->addSql('ALTER TABLE cours_suivi DROP FOREIGN KEY FK_1EDA4359BBA93DD6');
        $this->addSql('ALTER TABLE cours DROP FOREIGN KEY FK_FDCA8C9C2534008B');
        $this->addSql('ALTER TABLE diplome DROP FOREIGN KEY FK_EB4C4D4E79482D95');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649CEF8E52B');
        $this->addSql('ALTER TABLE cotisation DROP FOREIGN KEY FK_AE64D2ED67B3B43D');
        $this->addSql('ALTER TABLE cours DROP FOREIGN KEY FK_FDCA8C9C9E1E1A42');
        $this->addSql('ALTER TABLE cours_suivi DROP FOREIGN KEY FK_1EDA4359A76ED395');
        $this->addSql('ALTER TABLE diplome DROP FOREIGN KEY FK_EB4C4D4EA76ED395');
        $this->addSql('ALTER TABLE formation DROP FOREIGN KEY FK_404021BFA76ED395');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAF624B39D');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAE92F8F78');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE user_cours DROP FOREIGN KEY FK_1F0877C4A76ED395');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE cotisation');
        $this->addSql('DROP TABLE cours');
        $this->addSql('DROP TABLE cours_suivi');
        $this->addSql('DROP TABLE delegation');
        $this->addSql('DROP TABLE diplome');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE formation_prerequis');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE payment');
        $this->addSql('DROP TABLE payment_token');
        $this->addSql('DROP TABLE prerequis');
        $this->addSql('DROP TABLE prerogative');
        $this->addSql('DROP TABLE prerogative_formation');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE scheduled_command');
        $this->addSql('DROP TABLE stagiaire');
        $this->addSql('DROP TABLE structure');
        $this->addSql('DROP TABLE upload_document_formation');
        $this->addSql('DROP TABLE upload_image_diplome');
        $this->addSql('DROP TABLE upload_image_user');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_cours');
    }
}
