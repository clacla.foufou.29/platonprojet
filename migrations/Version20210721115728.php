<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210721115728 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cours_suivi DROP FOREIGN KEY FK_1EDA4359BBA93DD6');
        $this->addSql('DROP INDEX IDX_1EDA4359BBA93DD6 ON cours_suivi');
        $this->addSql('ALTER TABLE cours_suivi DROP stagiaire_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cours_suivi ADD stagiaire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cours_suivi ADD CONSTRAINT FK_1EDA4359BBA93DD6 FOREIGN KEY (stagiaire_id) REFERENCES stagiaire (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1EDA4359BBA93DD6 ON cours_suivi (stagiaire_id)');
    }
}
