<?php

namespace App\Command;

use App\Entity\User;
use League\Csv\Reader;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreateUserFromCSV2Command extends Command
{

    private EntityManagerInterface $entityManager;
    private String $dataDirectory;
    private UserRepository $userRepository;
    private SymfonyStyle $io;
    protected static $defaultName = 'app:create-user-from-csv2';
    protected static $defaultDescription = 'Import de données CSV';

    public function __construct(
        EntityManagerInterface $entityManager,
        string $dataDirectory,
        UserRepository $userRepository
    )
    {
        parent::__construct();
        $this->dataDirectory = $dataDirectory;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Attempting import of Feed...');

        $csv = Reader::createFromPath(fopen('public/data/data_user.csv', 'r+'));
        $csv->setHeaderOffset(0);
        //$results = $reader->fetchAll();
        /*foreach ($results as $row) {

            // create new athlete
            $user = (new User())
                ->setNumAdh($row['n°Adherent'])
                ->setNom($row['Nom'])
                ->setPrenom($row['Prénom'])
                ->setEmail($row['Adresse mail'])
                ->setDateNaissance($row['date de naissance'])
                ->setLieuDeNaissance($row['ville de naissance'])
                ->setDepartementDeNaissance($row['ville de naissance'])
            ;

            $this->em->persist($user);
            $io->progressAdvance();
        }*/

        //$this->em->flush();
        //$io->progressFinish();
        $io->success('Command exited cleanly!');
    }

}
