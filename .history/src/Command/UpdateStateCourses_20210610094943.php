<?php

namespace App\Command;

use DateTime;
use App\Entity\User;
use DateTimeInterface;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UpdateStateCourses extends Command
{

    private SymfonyStyle $io;
    protected static $defaultName = 'app:update-state-cours';
    protected static $defaultDescription = 'Mise à jour de l etat du cours';

    public function __construct($projectDir, EntityManagerInterface $entityManager)
    {
        $this->projectDir = $projectDir;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('update-state-cours')
        ->setDescription('Mise à jour de l etat du cours')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
       
        return Command::SUCCESS;
    }

    private function updateStateCourses(): void
    {
        $sqlQuery = "SET GLOBAL event_scheduler = 1;
                    CREATE DEFINER=`root`@`localhost`
                    CREATE EVENT event
                    ON SCHEDULE EVERY 1 DAY STARTS NOW()
                    DO
                    BEGIN
                        DECLARE
                            IF 
                    "
    }

}
