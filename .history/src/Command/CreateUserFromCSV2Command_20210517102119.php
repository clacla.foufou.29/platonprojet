<?php

namespace App\Command;

use App\Entity\User;
use League\Csv\Reader;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreateUserFromCSV2Command extends Command
{

    private EntityManagerInterface $entityManager;
    private String $dataDirectory;
    private UserRepository $userRepository;
    private SymfonyStyle $io;
    protected static $defaultName = 'app:create-user-from-csv2';
    protected static $defaultDescription = 'Import de données CSV';

    protected function configure(): void
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('import:user')
        ->setDescription('Import prescribers from .csv file')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Show when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        // Import CSV on DB via Doctrine ORM
        $this->import($input, $output);

        // Show when the script is over
        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
      $em = $this->getContainer()->get('doctrine')->getManager();

      // Turning off doctrine default logs queries for saving memory
      $em->getConnection()->getConfiguration()->setSQLLogger(null);

      // Get php array of data from CSV
      $data = $this->getData();

      // Start progress
      $size = count($data);
      $progress = new ProgressBar($output, $size);
      $progress->start();

      // Processing on each row of data
      $batchSize = 100; # frequency for persisting the data
      $i = 1;               # current index of records

      foreach($data as $row) {
         $p = $em->getRepository('AppBundle:Prescriber')->findOneBy(array(
                'rpps'       => $row['rpps'],
                'lastname'   => $row['nom'],
                'firstname'  => $row['prenom'],
                'profCode'   => $row['code_prof'],
                'postalCode' => $row['code_postal'],
                'city'       => $row['ville'],
         ));

         # If the prescriber doest not exist we create one
         if(!is_object($p)){
            $p = new Prescriber();
            $p->setRpps($row['rpps']);
            $p->setLastname($row['nom']);
            $p->setFirstname($row['prenom']);
            $p->setProfCode($row['code_prof']);
            $p->setPostalCode($row['code_postal']);
            $p->setCity($row['ville']);
            $em->persist($p);
         }    

         # flush each 100 prescribers persisted
         if (($i % $batchSize) === 0) {
            $em->flush();
            $em->clear();   // Detaches all objects from Doctrine!

            // Advancing for progress display on console
            $progress->advance($batchSize);
            $progress->display();
         }
         $i++;
      }

      // Flushing and clear data on queue
      $em->flush();
      $em->clear();

      // Ending the progress bar process
      $progress->finish();
    }

    protected function getData()
    {
        // Getting the CSV from filesystem
        $fileName = 'web/docs/prescripteurs.csv';

        // Using service for converting CSV to PHP Array
        $converter = $this->getContainer()->get('app.csvtoarray_converter');
        $data = $converter->convert($fileName);

        return $data;
    }

}
