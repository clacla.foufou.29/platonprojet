<?php

namespace App\Command;

use DateTime;
use App\Entity\User;
use DateTimeInterface;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UpdateStateCourses extends Command
{

    private SymfonyStyle $io;
    protected static $defaultName = 'app:update-state-cours';
    protected static $defaultDescription = 'Mise à jour de l etat du cours';

    public function __construct($projectDir, EntityManagerInterface $entityManager)
    {
        $this->projectDir = $projectDir;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('update-state-cours')
        ->setDescription('Mise à jour de l etat du cours')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
       $processFile = $input->getArgument('inputFile');
       $usersData = $this->getCsvRowAsArrays($processFile);

       $userRepo = $this->entityManager->getRepository(User::class);

       foreach($usersData as $userData){

           if( ! ( $existingUser = $userRepo->findOneBy(['numAdh' => $userData['nAdherent']]))) {
            echo 'hello';
               $this->NewUser($userData);
                continue;
               //dd($existingUser);
           }
        }
        $this->entityManager->flush();

        $io = new SymfonyStyle($input, $output);
        $io->success('it worked');
        return Command::SUCCESS;
    }

}
