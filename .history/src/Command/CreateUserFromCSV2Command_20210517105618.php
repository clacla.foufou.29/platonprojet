<?php

namespace App\Command;

use App\Entity\User;
use League\Csv\Reader;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreateUserFromCSV2Command extends Command
{

    private SymfonyStyle $io;
    protected static $defaultName = 'app:create-user-from-csv2';
    protected static $defaultDescription = 'Import de données CSV';

    protected function configure(): void
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('import:user')
        ->setDescription('Import prescribers from .csv file')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Import CSV on DB via Doctrine ORM
        $this->import($input, $output);
        return Command::SUCCESS;
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
        $csv = new SplFileObject('public/data/data-user.csv', 'r');

        /*
        Tell the object that this file should
        be treated as a CSV
        */
        $csv->setFlags(SplFileObject::READ_CSV);

        /*
        Wrap the CSV file in a filter which
        skips the first line
        */
        $csv = new LimitIterator($csv, 1);

        /*
        Loops over the CSV file, the SplFileObject
        automatically creates an array for you
        */
        foreach($csv as $line){
        var_dump($line); #it's a numerically indexed array
        }
    }

}
