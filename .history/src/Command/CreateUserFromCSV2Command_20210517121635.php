<?php

namespace App\Command;

use LimitIterator;
use SplFileObject;
use App\Entity\User;
use League\Csv\Reader;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreateUserFromCSV2Command extends Command
{

    private SymfonyStyle $io;
    protected static $defaultName = 'app:create-user-from-csv2';
    protected static $defaultDescription = 'Import de données CSV';

    public function __construct($projectDir, EntityManagerInterface $entityManager)
    {
        $this->projectDir = $projectDir;
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('import:user')
        ->setDescription('Import prescribers from .csv file')
        ->addArgument('inputFile', InputArgument::REQUIRED, 'Fichier CSV')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
       $processFile = $input->getArgument('inputFile');
       $usersData = $this->getCsvRowAsArrays($processFile);

       $userRepo = $this->entityManager->getRepository();

       foreach($usersData as $userData){

       }

        return Command::SUCCESS;
    }

    public function getCsvRowAsArrays($processFile){
        $inputFile = $this->projectDir . '/public/data/' . $processFile . '.csv';
        $decoder = new Serializer([new ObjectNormalizer()], [new CsvEncoder()])
        return $decoder->decode(file_get_contents($inputFile), 'csv');
    }

}
