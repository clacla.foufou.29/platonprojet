<?php

namespace App\Command;

use App\Entity\User;
use League\Csv\Reader;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreateUserFromCSV2Command extends Command
{

    private SymfonyStyle $io;
    protected static $defaultName = 'app:create-user-from-csv2';
    protected static $defaultDescription = 'Import de données CSV';

    protected function configure(): void
    {
        $this
        // the name of the command (the part after "bin/console")
        ->setName('import:user')
        ->setDescription('Import prescribers from .csv file')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Import CSV on DB via Doctrine ORM
        $this->import($input, $output);
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
            // Read a CSV file
        $handle = fopen("my_huge_csv_file.csv", "r");

        // Optionally, you can keep the number of the line where
        // the loop its currently iterating over
        $lineNumber = 1;

        // Iterate over every line of the file
        while (($raw_string = fgets($handle)) !== false) {
            // Parse the raw csv string: "1, a, b, c"
            $row = str_getcsv($raw_string);

            // into an array: ['1', 'a', 'b', 'c']
            // And do what you need to do with every line
            var_dump($row);
            
            // Increase the current line
            $lineNumber++;
        }

        fclose($handle);
    }

}
