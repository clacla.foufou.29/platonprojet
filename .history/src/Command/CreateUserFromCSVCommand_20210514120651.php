<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreateUserFromCSVCommand extends Command
{

    private EntityManagerInterface $entityManager;
    private String $dataDirectory;
    private UserRepository $userRepository;
    private SymfonyStyle $io;
    protected static $defaultName = 'app:create-user-from-csv';
    protected static $defaultDescription = 'Import de données CSV';

    public function __construct(
        EntityManagerInterface $entityManager,
        string $dataDirectory,
        UserRepository $userRepository
    )
    {
        parent::__construct();
        $this->dataDirectory = $dataDirectory;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->createUsers();

        return Command::SUCCESS;
    }

    private function getDataFromFile(): array
    {
        $file = $this->dataDirectory . 'data_user.csv';
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);

        $normalizers = [new ObjectNormalizer()];
        $encoder = [
            new CsvEncoder()
        ];
        $serializer = new Serializer($normalizers, $encoder);

        /**
         * @var string $fileString
         */
        $fileString = file_get_contents($file);

        $data = $serializer->decode($fileString, $fileExtension);
        if(array_key_exists('results', $data)){
            return $data['results'];
        }
        return $data;
    }

    private function createUsers():void
    {
        $this->io->section('Creation des utilisateurs a partir du fichier');
        $usersCreated = 0;
        foreach($this->getDataFromFile() as $row){
            if(array_key_exists('email', $row) && !empty($row['email'])){
                $user = $this->userRepository->findOneBy([
                    'email' => $row['email']
                ]);

                if(!$user){
                    $user = new User();
                    $user->setEmail($row['Adresse email'])
                        ->setPassword('badPassword');
                        $this->entityManager->persist($user);
                        $usersCreated++;
                }
            }
        }
        $this->entityManager->flush();
        if($usersCreated>=1){
            $string = "{$usersCreated} Utilisateur crée";
        } else {
            $string ="pas d'utilisateur ";
        }
        $this->io->success($string);
    }
}
