<?php

namespace App\EventListener;

use DateTime;
use App\Entity\User;
use App\Entity\Diplome;
use App\Entity\Notification;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class EmailAfterAccountCreate
{

    private $security;
    protected $mailer;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /*public function getSubscribedEvents(): array
    {
        return [
            //BeforeEntityPersistedEvent::class => 'setNotificationDate',
            //AfterEntityDeletedEvent::class => 'postRemove',
            Events::postRemove,
        ];
    }*/


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        $user = $entity->getUser();
        $sender = $this->security->getUser();

    }

    /*public function setNotificationDate(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Notification)) {
            return;
        }

        $now = new DateTime('now');
        $entity->setDateCreation($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }

    public function sendNotificationDiplome(AfterEntityDeletedEvent $event)
    {
        

    }*/

}