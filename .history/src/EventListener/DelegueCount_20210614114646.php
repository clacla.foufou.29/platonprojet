<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\Delegation;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class DelegueCount
{

    private $security;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function prePersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();
        $entityManager = $this->getEntityManager();

        if((! ($entity instanceof User) )|| (! ($entity instanceof Delegation) )){
            return;
        }
        //$delegation = array();
        $delegation = $this->getDoctrine()->getRepository(Delegation::class)->countDeleguePerDelegation();

        foreach ($results as $result) {
            $delegation = $result[0];
            $delegation->setDelegue($result[1]);

            $delegations[] = $delegation;
        }

        $em = $event->getObjectManager();
        $em->persist($delegations);
        $em->flush();
    }


}