<?php

namespace App\EventListener;

use DateTime;
use App\Entity\User;
use App\Entity\Diplome;
use App\Entity\Notification;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class EmailAfterAccountCreate
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /*public function getSubscribedEvents(): array
    {
        return [
            //BeforeEntityPersistedEvent::class => 'setNotificationDate',
            //AfterEntityDeletedEvent::class => 'postRemove',
            Events::postRemove,
        ];
    }*/


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        $user = $entity->getUser();
        $sender = $this->security->getUser();

    }

    public function sendEmail(MailerInterface $mailer, User $user){
        $email = new (Email())
        -> from('platon@admin.fr')
        -> to($user->getEmail())
        -> subject('Bienvenue sur Platon')
        -> html('<p>Votre compte a été créé ! </p>');

        $mailer->send($email);

        return new Response('Email envoyé');

}