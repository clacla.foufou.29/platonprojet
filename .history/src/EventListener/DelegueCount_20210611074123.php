<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\Delegation;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class DelegueCount
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        $user = $entity->getDelegation();
        $delegationRepository = $this->entityManager->getRepository(Delegation::class);
        $delegation = $delegationRepository->findAll();

        $em = $event->getObjectManager();
        $em->persist($notification);
        $em->flush();
    }

    /*public function setNotificationDate(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Notification)) {
            return;
        }

        $now = new DateTime('now');
        $entity->setDateCreation($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }

    public function sendNotificationDiplome(AfterEntityDeletedEvent $event)
    {
        

    }*/

}