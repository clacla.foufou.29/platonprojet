<?php

namespace App\EventListener;

use DateTime;
use App\Entity\User;
use App\Entity\Diplome;
use App\Entity\Notification;
use Symfony\Component\Mime\Email;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class EmailAfterAccountCreate
{

    private $security;
    private $mailer;

    public function __construct(Security $security, MailerInterface $mailer)
    {
        $this->security = $security;
        $this->mailer = $mailer;
    }

    /*public function getSubscribedEvents(): array
    {
        return [
            //BeforeEntityPersistedEvent::class => 'setNotificationDate',
            //AfterEntityDeletedEvent::class => 'postRemove',
            Events::postRemove,
        ];
    }*/


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        

        $this->mailer->send((new Email())
                -> from('platon@admin.fr')
                -> subject('Bienvenue sur Platon')
                -> to($user->getEmail())
                ->htmlTemplate('registration/confirmation_email.html.twig')
            );

    }

}