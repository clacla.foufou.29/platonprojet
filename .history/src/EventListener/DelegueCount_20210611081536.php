<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\Delegation;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Persistence\Event\PreUpdateEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class DelegueCount
{

    private $security;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        $delegationUser = $entity->getDelegation();
        $delegationRepository = $this->entityManager->getRepository(Delegation::class);
        $delegations = $delegationRepository->findAll();
        foreach($delegations as $delegation){
            if( $delegationUser->getId() == $delegation->getId()){
                $delegation->setDelegue($delegation->getDelegue()+1);
                $this->entityManager->persist($delegation);
            }
        }
        $this->entityManager->flush();
    }

    public function preUpdate(PreUpdateEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        $delegationUser = $entity->getDelegation();
        $delegationRepository = $this->entityManager->getRepository(Delegation::class);
        $delegations = $delegationRepository->findAll();
        foreach($delegations as $delegation){
            if( $delegationUser->getId() == $delegation->getId()){
                if($delegationUser->hasChangedField('Id')){
                    $oldDelegation = $delegationUser->getOldValue('Id');
                    $oldDelegation->setDelegue($oldDelegation->getDelegue()-1);
                    $delegation->setDelegue($delegation->getDelegue()+1);

                    $this->entityManager->persist($delegation);
                    $this->entityManager->persist($oldDelegation);
                }
                
            }
        }
        $this->entityManager->flush();
    }


}