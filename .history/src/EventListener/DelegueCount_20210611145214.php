<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\Delegation;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class DelegueCount
{

    private $security;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postPersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();
        $entityManager = $this->getEntityManager();

        if(($entity instanceof User) || ($entity instanceof Delegation) ){
            
                //$nbreDelegue = $delegationRepository->createQueryBuilder('d')
                $query = $entityManager->createQuery(" SELECT COUNT(*) FROM Delegation INNER JOIN User on Delegation.id=user.delegation_id GROUP BY delegation.id");
                $result = $query->getResult();
            $this->entityManager->flush();
        }
    }


}