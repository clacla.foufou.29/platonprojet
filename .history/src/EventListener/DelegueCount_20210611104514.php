<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\Delegation;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class DelegueCount
{

    private $security;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function prePersist(User $user, LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        $delegationUser = $entity->getDelegation();
        $delegationRepository = $this->entityManager->getRepository(Delegation::class);
        $delegations = $delegationRepository->findAll();
        foreach($delegations as $delegation){
            if( $delegationUser->getId() == $delegation->getId()){
                $delegation->setDelegue($delegation->getDelegue()+1);
                $this->entityManager->persist($delegation);
            }
        }
        $this->entityManager->flush();
    }

    /*public function preUpdate(User $user, PreUpdateEventArgs $event): void
    {
        $entity = $event->getObject();

        if(!($entity instanceof User)){
            return;
        }

        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        $delegationUser = $entity->getDelegation();
        $delegationRepository = $this->entityManager->getRepository(Delegation::class);
        $delegations = $delegationRepository->findAll();
        foreach($delegations as $delegation){
            if( $delegationUser->getId() == $delegation->getId()){
                
                if($event->hasChangedField('delegation')){
                    dd($oldDelegation);
                    $oldDelegation = $delegationUser->getOldValue('Id');
                    $oldDelegation->setDelegue($oldDelegation->getDelegue()-1);
                    $delegation->setDelegue($delegation->getDelegue()+1);
                }
            }
        }
        $this->entityManager->flush();
    }*/


}