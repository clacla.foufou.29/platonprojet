<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\Delegation;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;

class DelegueCount implements EventSubscriberInterface
{

    private $security;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function prePersist(LifecycleEventArgs $event): void
    {
        $entity = $event->getObject();
        $entityManager = $this->getEntityManager();

        if((! ($entity instanceof User) ) && (! ($entity instanceof Delegation) )){
            return;
        }
        //$delegation = array();
        $delegations = $this->getDoctrine()->getRepository(Delegation::class);
        
        foreach ($delegations as $delegation) {
            dd($delegation);
        }

        $em = $event->getObjectManager();
        $em->persist($delegation);
        $em->flush();
    }


}