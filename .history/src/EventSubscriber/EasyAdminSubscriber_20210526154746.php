<?php

namespace App\EventSubscriber;

use App\Entity\Notification;
use Doctrine\Common\EventSubscriber;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class NotificationCrudController implements EventSubscriber
{

    private $user;

    public function __construct(Security $user)
    {

    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setNotificationDate'],
        ];
    }

    public function setNotificationDate(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Notification)) {
            return;
        }

        $now = new DateTime('now');
        $entity->setDateCreation($now);
    }

}