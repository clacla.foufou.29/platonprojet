<?php

namespace App\EventSubscriber;

use DateTime;
use App\Entity\Notification;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Security\Core\Security;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class NotificationCrudController implements EventSubscriberInterface
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setNotificationDate'],
        ];
    }

    public function setNotificationDate(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Notification)) {
            return;
        }

        $now = new DateTime('now');
        $entity->setDateCreation($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }

}