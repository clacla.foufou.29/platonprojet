<?php

namespace App\EventSubscriber;

use DateTime;
use App\Entity\Notification;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class NotificationController implements EventSubscriberInterface
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


     // the entity listener methods receive two arguments:
    // the entity instance and the lifecycle event
    public function postRemove(Diplome $diplome, LifecycleEventArgs $event): void
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Diplome)){
            return;
        }

        $user = $entity->getUser();
        $sender = $this->security->getUser();

        $now = new DateTime('now');

        $notification = new Notification();
        $notification->setTitre("Diplome refusé");
        $notification->setMessage("");
        $notification->setDateCreation($now);
        $notification->setVue(true);
        $notification->setSender($sender);
        $notification->setRecipient($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($notification);
        $em->flush();
    }
    

    /*public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => 'setNotificationDate',
            AfterEntityDeletedEvent::class => 'sendNotificationDiplome',
        ];
    }*/

    public function setNotificationDate(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Notification)) {
            return;
        }

        $now = new DateTime('now');
        $entity->setDateCreation($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }

    public function sendNotificationDiplome(AfterEntityDeletedEvent $event)
    {
        

    }

}