<?php

namespace App\EventSubscriber;

use DateTime;
use App\Entity\Notification;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Security\Core\Security;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class NotificationCrudController implements EventSubscriberInterface
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setNotificationDate'],
            AfterEntityDeletedEvent::class => ['sendNotificationDiplome'],
        ];
    }

    public function setNotificationDate(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Notification)) {
            return;
        }

        $now date_create()->format('Y-m-d H:i:s');
        $entity->setDateCreation($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }
    public function sendNotificationDiplome(AfterEntityDeletedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Diplome)){
            return;
        }

        $user = $entity->getUser();
        $sender = $this->security->getUser();

        $now = new DateTime('now');

        $notification = new Notification();
        $notification->setTitre("Diplome refusé");
        $notification->setMessage("");
        $notification->setDateCreation($now);
        $notification->setVue(true);
        $notification->setSender($sender);
        $notification->setRecipient($user);

    }

}