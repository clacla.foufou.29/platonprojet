<?php

namespace App\EventSubscriber;

use App\Entity\Notification;
use Doctrine\Common\EventSubscriber;

class NotificationCrudController implements EventSubscriber
{

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setNotificationDate'],
        ];
    }

    public function setNotificationDat(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof Notification)) {
            return;
        }
    }

}