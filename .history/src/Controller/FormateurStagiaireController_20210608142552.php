<?php

namespace App\Controller;

use App\Form\StagiaireType;
use App\Repository\StagiaireRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_FORMATEUR")
 * @Route("/formateur/stagiaire", name="formateur_stagiaire_")
 * @package App\Controller
* /
class FormateurStagiaireController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(StagiaireRepository $stagiaires): Response
    {
        return $this->render('formateur_stagiaire/index.html.twig', [
            'stagiaires' => $stagiaires->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function creationStagiaire(Request $request)
    {
        $stagiaire = new Stagiaire;

        $form = $this->createForm(StagiaireType::class, $stagiaire);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($stagiaire);
            $em->flush();
            return $this->redirectToRoute('formateur_stagiaire_home');
        }

        return $this->render('cours/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }


    #[Route('/cours/edit/{id}', name:'cours_edit')]
    public function editCours(Cours $cours, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(CoursType::class, $cours);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($cours);
            $em->flush();

            return $this->redirectToRoute('cours');
        }

        return $this->render('cours/ajout.html.twig', [
            'form' => $form->createView(),
            'cours' => $cours
        ]);
    }

    #[Route('/cours/supprimer/{id}', name:'cours_supprimer')]
    public function supprimerCours(Cours $cours)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($cours);
        $em->flush();

        $this->addFlash('message', 'Cours supprimé avec succès');
        return $this->redirectToRoute('cours');
    }
}
