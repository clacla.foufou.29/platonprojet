<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Form\FormationType;
use App\Repository\FormationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FormationController extends AbstractController
{
    #[Route('/formation', name: 'formation')]
    public function index(FormationRepository $formation): Response
    {
        return $this->render('formation/index.html.twig', [
            'formations' => $formation->findAll(),
        ]);
    }
    #[Route('/formation/ajout', name:'formation_ajout')]
    public function ajoutFormation(Request $request)
    {
        $formation = new Formation;

        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute('formation');
        }

        return $this->render('formation/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
