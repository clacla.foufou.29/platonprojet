<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormateurStagiaireController extends AbstractController
{
    #[Route('/formateur/stagiaire', name: 'formateur_stagiaire')]
    public function index(): Response
    {
        return $this->render('formateur_stagiaire/index.html.twig', [
            'controller_name' => 'FormateurStagiaireController',
        ]);
    }
}
