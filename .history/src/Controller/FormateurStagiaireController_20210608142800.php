<?php

namespace App\Controller;

use App\Form\StagiaireType;
use App\Repository\StagiaireRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_FORMATEUR")
 * @Route("/formateur/stagiaire", name="formateur_stagiaire_")
 * @package App\Controller
* /
class FormateurStagiaireController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(StagiaireRepository $stagiaires): Response
    {
        return $this->render('formateur_stagiaire/index.html.twig', [
            'stagiaires' => $stagiaires->findAll(),
        ]);
    }

    #[Route('/creation', name:'creation')]
    public function creationStagiaire(Request $request)
    {
        $stagiaire = new Stagiaire;

        $form = $this->createForm(StagiaireType::class, $stagiaire);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($stagiaire);
            $em->flush();
            return $this->redirectToRoute('formateur_stagiaire_home');
        }

        return $this->render('formateur_stagiaire/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/cours/supprimer/{id}', name:'cours_supprimer')]
    public function supprimerCours(Cours $cours)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($cours);
        $em->flush();

        $this->addFlash('message', 'Cours supprimé avec succès');
        return $this->redirectToRoute('cours');
    }
}
