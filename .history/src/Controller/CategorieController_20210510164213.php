<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoriController extends AbstractController
{
    #[Route('/categori', name: 'categori')]
    public function index(): Response
    {
        return $this->render('categori/index.html.twig', [
            'controller_name' => 'CategoriController',
        ]);
    }
}
