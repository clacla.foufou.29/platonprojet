<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Entity\Stagiaire;
use App\Form\StagiaireType;
use App\Repository\StagiaireRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_FORMATEUR")
 * @Route("/formateur/stagiaire", name="formateur_stagiaire_")
 * @package App\Controller
 */
class FormateurStagiaireController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(StagiaireRepository $stagiaires): Response
    {
        return $this->render('formateur_stagiaire/index.html.twig', [
            'stagiaires' => $stagiaires->findAll(),
        ]);
    }

    #[Route('/creation', name:'creation')]
    public function creationStagiaire(Request $request)
    {
        $stagiaire = new Stagiaire;

        $form = $this->createForm(StagiaireType::class, $stagiaire);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($stagiaire);
            $em->flush();
            return $this->redirectToRoute('formation');
        }

        return $this->render('formateur_stagiaire/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }
    #[Route('/ajoutStagiaireFormation/{id}', name:'ajout_stagiaire_formation')]
    public function ajoutStagiaireFormation(Request $request, Formation $formation)
    {

        $form = $this->createForm(FormationStagiaireType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $stagiaires = $form->get('stagiaire')->getData();

            // On boucle sur les images
            foreach($stagiaires as $stagiaire){
                $stagiaire->addFormation($formation);
                $formation->addStagiaire($stagiaire);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($stagiaire);
            $em->flush();
            return $this->redirectToRoute('formation');
        }

        return $this->render('formateur_stagiaire/ajoutList.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
