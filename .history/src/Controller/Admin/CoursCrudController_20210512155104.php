<?php

namespace App\Controller\Admin;

use App\Entity\Cours;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class CoursCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Cours::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)

        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $nomCours = TextField::new('nomCours');
        $ville = TextField::new('ville');
        $dateDebut = DateTimeField::new('dateDebut');
        $dateFin = DateTimeField::new('dateFin');
        $nbStagiaire = IntegerField::new('nbStagiaire');
        $cp = IntegerField::new('cp');
        $adresse = TextField::new('adresse');
        //$validationCours = IntegerField::new('nbStagiaires');
        $formation = AssociationField::new('formation');
        $structure = AssociationField::new('structure');
        $delegation = AssociationField::new('delegation');
        $formateurs = AssociationField::new('formateurs');
        //$formateurPrincipal = AssociationField::new('$formateurPrincipal');
        if (Crud::PAGE_INDEX === $pageName) {
            return [$nomCours, $ville, $dateDebut, $dateFin, $nbStagiaires];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$nomCours, $ville, $dateDebut, $dateFin, $nbStagiaires,$cp, $adresse, $validationCours,
            $formation, $structure, $formateurs];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$nomCours, $ville, $dateDebut, $dateFin, $nbStagiaires,$cp, $adresse, $validationCours,
            $formation, $structure, $formateurs];
        } elseif(Crud::PAGE_EDIT === $pageName) {
                return [$nomCours, $ville, $dateDebut, $dateFin, $nbStagiaires,$cp, $adresse, $validationCours,
                $formation, $structure, $formateurs];
        } else {
            return [$nomCours, $ville, $dateDebut, $dateFin, $nbStagiaires];
        }
    }

}
