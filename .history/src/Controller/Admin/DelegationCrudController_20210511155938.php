<?php

namespace App\Controller\Admin;

use App\Entity\Delegation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class DelegationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Delegation::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)

        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $nomDelegation = TextField::new('nomDelegation');
        $departement = TextField::new('departement');
        //$medecins = AssociationField::new('medecins');
        $delegue = IntegerField::new('delegue');
        if (Crud::PAGE_INDEX === $pageName) {
            return [$nomDelegation, $departement, $delegue];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$nomDelegation, $departement, $delegue];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$nomDelegation, $departement, $delegue];
        } elseif(Crud::PAGE_EDIT === $pageName) {
            return [$nomDelegation, $departement, $delegue];
        } else {
            return [$nomDelegation, $departement, $medecins, $delegue];
        }
    }
}
