<?php

namespace App\Controller\Admin;

use App\Entity\Diplome;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DiplomeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Diplome::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)

        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $libelleFormation = TextField::new('libelleFormation');
        $dateDebutFormation = DateField::new('dateDebutFormation');
        $dateFinFormation = DateField::new('dateFinFormation');
        $diplomeFile = TextField::new('diplomeFile', 'Fichier du diplome')->setFormType(VichImageType::class, [
            'required' => false,
        ])->onlyWhenCreating();
        $imageDiplome = ImageField::new('file')->setBasePath('/uploads/diplomes/')->onlyOnIndex();
        $status = TextField::new('status');
        $montantDiplome = IntegerField::new('montantDiplome');
        $formation = AssociationField::new('formation');
        $user = AssociationField::new('user', 'Adherent');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$libelleFormation, $dateDebutFormation, $dateFinFormation, $montantDiplome, $imageDiplome];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$libelleFormation, $dateDebutFormation, $dateFinFormation, $montantDiplome,
            $diplomeFile,$imageField, $formation, $user];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$libelleFormation, $dateDebutFormation, $dateFinFormation, $montantDiplome,
            $diplomeFile, $formation, $user];
        } elseif(Crud::PAGE_EDIT === $pageName) {
            return [$libelleFormation, $dateDebutFormation, $dateFinFormation, $montantDiplome,
            $diplomeFile,$formation, $user];
        } else {
            return [$libelleFormation, $dateDebutFormation, $dateFinFormation, $montantDiplome];
        }
    }
}
