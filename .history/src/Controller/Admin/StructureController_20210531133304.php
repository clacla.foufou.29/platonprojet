<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Structure;
use App\Form\StructureType;
use App\Repository\StructureRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/structure", name="admin_structure_")
 * @package App\Controller\Admin
 */
class StructureController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(StructureRepository $structure): Response
    {
        return $this->render('admin/structure/index.html.twig', [
            'structures' => $structure->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutStructure(Request $request)
    {
        $structure = new Structure;

        $form = $this->createForm(StructureType::class, $structure);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($structure);
            $em->flush();
            return $this->redirectToRoute('admin_structure_home');
        }

        return $this->render('admin/structure/ajout.html.twig', [
            'structure_form' => $form->createView()
        ]);
    }

    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Structure $structure, StructureRepository $structureRepository): Response
    {
        return new Response($twig->render('admin/structure/show.html.twig', [
            'structure' => $structure,
        ]));
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editStructure(Structure $structure, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(StructureType::class, $structure);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($structure);
            $em->flush();

            return $this->redirectToRoute('admin_structure_home');
        }

        return $this->render('admin/structure/ajout.html.twig', [
            'structure_form' => $form->createView(),
            'structure' => $structure
        ]);
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerStructure(Structure $structure)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($structure);
        $em->flush();

        $this->addFlash('message', 'Structure supprimé avec succès');
        return $this->redirectToRoute('admin_structure_home');
    }
}