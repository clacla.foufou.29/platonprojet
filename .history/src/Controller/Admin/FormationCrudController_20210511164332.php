<?php

namespace App\Controller\Admin;

use App\Entity\Formation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class FormationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Formation::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)

        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $libelleFormation = TextField::new('libelleFormation');
        $age = IntegerField::new('age');
        $nbMinStagiaires = IntegerField::new('nbMinStagiaire');
        $nbMaxStagiaires = IntegerField::new('nbMaxStagiaire');
        $nbFormateurs = IntegerField::new('nbFormateurs');
        $prixFormation = IntegerField::new('prixFormation');
        $validationFormation = IntegerField::new('validationFormation');
        $categorie = AssociationField::new('categorie');
        $diplome = AssociationField::new('diplome');
        $cours = AssociationField::new('cours');
        $prerogatives = AssociationField::new('prerogatives');
        $prerequis = AssociationField::new('prerequis');
        if (Crud::PAGE_INDEX === $pageName) {
            return [$libelleFormation, $nbFormateurs, $validationFormation, $categorie];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$libelleFormation, $age, $nbMinStagiaires, $nbMaxStagiaires, $nbFormateurs, $prixFormation, $validationFormation, $categorie, $diplome, $cours, $prerogatives, $prerequis];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$libelleFormation, $age, $nbMinStagiaires, $nbMaxStagiaires, $nbFormateurs, $prixFormation, $validationFormation, $categorie, $diplome, $cours, $prerogatives, $prerequis];
        } elseif(Crud::PAGE_EDIT === $pageName) {
            return [$libelleFormation, $age, $nbMinStagiaires, $nbMaxStagiaires, $nbFormateurs, $prixFormation, $validationFormation, $categorie, $diplome, $cours, $prerogatives, $prerequis];
        } else {
            return [$libelleFormation, $nbFormateurs, $validationFormation, $categorie];
        }
    }
}
