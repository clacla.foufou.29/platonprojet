<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Diplome;
use App\Form\DiplomeType;
use App\Entity\UploadImageDiplome;
use App\Repository\DiplomeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/diplome", name="admin_diplome_")
 * @package App\Controller\Admin
 */
class DiplomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(DiplomeRepository $diplome): Response
    {
        return $this->render('admin/diplome/index.html.twig', [
            'diplomes' => $diplome->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutDiplome(Request $request)
    {
        $diplome = new Diplome;

        $form = $this->createForm(DiplomeType::class, $diplome);
        

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ){
            // On récupère les images transmises
            $image = $form->get('uploadImageDiplome')->getData();
            $extension = $image->guessExtension();
            $fichier = md5(uniqid()).'.'.$extension;
            $diplome->setAccept(false);
            // On copie le fichier dans le dossier uploads
            $image->move($this->getParameter('diplome_images'),$fichier);
            // On crée l'image dans la base de données
            $img = new UploadImageDiplome();
            $img->setNom($fichier);
            $diplome->setUploadImageDiplome($img);
            $diplome->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($diplome);
            $em->flush();
            return $this->redirectToRoute('admin_diplome_home');
        }

        return $this->render('admin/diplome/ajout.html.twig', [
            'diplome_form' => $form->createView()
        ]);
    }

    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Diplome $diplome, DiplomeRepository $diplomeRepository): Response
    {
        return new Response($twig->render('admin/diplome/show.html.twig', [
            'diplome' => $diplome,
        ]));
    }

    #[Route('/valider/{id}', name:'valider')]
    public function valider(Diplome $diplome)
    {
        $diplome->setAccept(($diplome->getAccept())?false:true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($diplome);
        $em->flush();

        return new Response("true");
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editDiplome(Diplome $diplome, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(DiplomeType::class, $diplome);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            if($form->get('uploadImageDiplome')->getData()!= null){
                $image = $form->get('uploadImageDiplome')->getData();
                $extension = $image->guessExtension();
                $fichier = md5(uniqid()).'.'.$extension;
                    
                // On copie le fichier dans le dossier uploads
                $image->move($this->getParameter('diplome_images'),$fichier);
                // On crée l'image dans la base de données
                $img = new UploadImageDiplome();
                $img->setNom($fichier);
                $diplome->setUploadImageDiplome($img);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($diplome);
            $em->flush();

            return $this->redirectToRoute('admin_diplome_home');
        }

        return $this->render('admin/diplome/edit.html.twig', [
            'diplome_form' => $form->createView(),
            'diplome' => $diplome
        ]);
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerDiplome(Diplome $diplome)
    {
        $images = $diplome->getUploadImageDiplome();
        
        if($images){
            // On "génère" le chemin physique de l'image
            $nomImage = $this->getParameter("diplome_images") . '/' . $images->getNom();
            // On vérifie si l'image existe
            if(file_exists($nomImage)){
                unlink($nomImage);

            }
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($diplome);
        $em->flush();

        $this->addFlash('message', 'Diplome supprimé avec succès');
        return $this->redirectToRoute('admin_diplome_home');
    }

}

