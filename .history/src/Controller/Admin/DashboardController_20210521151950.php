<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Cours;
use App\Entity\Diplome;
use App\Entity\Categorie;
use App\Entity\Formation;
use App\Entity\Prerequis;
use App\Entity\Structure;
use App\Entity\Cotisation;
use App\Entity\Delegation;
use App\Entity\Prerogative;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Platon');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Accueil', 'fa fa-home'),

            MenuItem::section('Paramétrage'),
            MenuItem::linkToCrud('Formations', 'fas fa-clapboard-teacher', Formation::class),
            MenuItem::linkToCrud('Cours', 'fas fa-school', Cours::class),
            MenuItem::linkToCrud('Structures', 'fas fa-adress-book', Structure::class),
            MenuItem::linkToCrud('Diplomes', 'fas fa-graduation-cap', Diplome::class),
            MenuItem::linkToCrud('Categories', 'fa fa-tags', Categorie::class),
            MenuItem::linkToCrud('Prerequis', 'fa fa-tags', Prerequis::class),
            MenuItem::linkToCrud('Prerogatives', 'fa fa-tags', Prerogative::class),
            MenuItem::linkToCrud('Cotisations', 'far fa-credit-card', Cotisation::class),

            MenuItem::section('Delegation'),
            MenuItem::linkToCrud('Delegation', 'fa fa-comment', Delegation::class),
            MenuItem::linkToCrud('Adhérent', 'fas fa-users', User::class),
            
        ];
    }
}
