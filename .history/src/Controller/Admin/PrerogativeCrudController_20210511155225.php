<?php

namespace App\Controller\Admin;

use App\Entity\Prerogative;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PrerogativeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Prerogative::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
