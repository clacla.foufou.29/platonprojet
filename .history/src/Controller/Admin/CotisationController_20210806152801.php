<?php

namespace App\Controller\Admin;

use Monolog\Logger;
use Twig\Environment;
use App\Entity\Cotisation;
use App\Form\CotisationType;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use App\Form\CotisationAdherentType;
use App\Repository\CotisationRepository;
use Lyra\PayzenBundle\Payzen\PayzenService;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Lyra\PayzenBundle\Includes\PayzenParamsUrl;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\Response\CurlResponse;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/cotisation", name="admin_cotisation_")
 * @package App\Controller\Admin
 */
class CotisationController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(CotisationRepository $cotisation): Response
    {
        return $this->render('Admin/cotisation/index.html.twig', [
            'cotisations' => $cotisation->findAll(),
        ]);
    }

   #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Cotisation $cotisation, CotisationRepository $cotisationRepository): Response
    {
        return new Response($twig->render('Admin/cotisation/show.html.twig', [
            'cotisation' => $cotisation,
        ]));
    }

    #[Route('/ajout', name: 'ajout')]
    public function new(Request $request): Response
    {
        $cotisation = new Cotisation;

        $form = $this->createForm(CotisationType::class, $cotisation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($cotisation);
            $em->flush();

            return $this->redirectToRoute('admin_cotisation_home');
        }

        return new Response($twig->render('Admin/cotisation/show.html.twig', [
            'cotisation' => $cotisation,
        ]));
    }

    /**
     * @Route("/cotisation/paiement/{id}", name="paiement_cotisation", methods = {"GET", "POST"})
     * 
    */
    public function paiementCotisation(Request $request, Cotisation $cotisation)
    {
        //$client = new HttpClientInterface();
        $httpClient = HttpClient::create();
        $form = $this->createForm(CotisationAdherentType::class, $cotisation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $cotisation->setUser($this->getUser());
            $moyen = $form->getData()->getMoyenDePaiement();
            $cotisation->setMoyenDePaiement($moyen);
            $data = $form->getData();
            $prix = $cotisation->getPrixCotisation()*100;
            $em = $this->getDoctrine()->getManager();
            $em->persist($cotisation);
            $em->flush();
            $response = new Response();
            //$response = new CurlResponse();
            $actionMode = $cotisation->getVadsActionMode();
            //$captureDelay = $cotisation->getVadsCaptureDelay();
            $ctxMode = $cotisation->getCtxMode();
            $currency = $cotisation->getVadsCurrency();
            $pageAction = $cotisation->getVadsPageAction();
            //$paymentCards = $cotisation->getVadsPaymentCards();
            $paymentConfig = $cotisation->getVadsPaymentConfig();
            $siteId = $cotisation->getVadsSiteId();
            $transDate = $cotisation->getVadsTransDate();
            $transId = $cotisation->getVadsTransId();
            $version = $cotisation->getVadsVersion();
            //$certificate = $cotisation->getCertificate();
            $signature = $cotisation->getSignature();
            return $response = $httpClient->request('POST', 'https://paiement.systempay.fr/vads-payment/', [
                'body' => [
                    'vads_action_mode' =>  $actionMode,
                    'vads_amount' => $prix,
                    //'vads_capture_delay' => $captureDelay,
                    'vads_ctx_mode' => $ctxMode,
                    'vads_currency' => $currency,
                    'vads_page_action' => $pageAction,
                    //'vads_payment_cards' => $paymentCards,
                    'vads_payment_config' => $paymentConfig,
                    'vads_site_id' => $siteId,
                    'vads_trans_date' => $transDate,
                    'vads_trans_id' => $transId,
                    'vads_version' => $version,
                    //'vads_certificate' => $certificate,
                    'signature' => $signature
                ]
            ]);
            //echo $response->getContent();
        }

        return $this->render('Admin/cotisation/paiement.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
