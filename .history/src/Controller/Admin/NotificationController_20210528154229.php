<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Notification;
use App\Form\NotificationType;
use App\Repository\NotificationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/notification", name="admin_notification_")
 * @package App\Controller\Admin
 */
class NotificationController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('admin/notification/index.html.twig', [
            'controller_name' => 'NotificationController',
        ]);
    }
    /**
     * @Route("/envoie", name="envoie")
     */
    public function send(Request $request): Response
    {
        $notification = new Notification;
        $form = $this->createForm(NotificationType::class, $notification);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $notification->setSender($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($notification);
            $em->flush();

            $this->addFlash("notification", "Notification envoyé avec succès.");
            return $this->redirectToRoute("admin_notification_home");
        }

        return $this->render("admin/notification/send.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/received", name="received")
     */
    public function received(): Response
    {
        return $this->render('admin/notification/received.html.twig');
    }


    /**
     * @Route("/sent", name="sent")
     */
    public function sent(): Response
    {
        return $this->render('admin/notification/sent.html.twig');
    }

    #[Route('/read/{id}', name: 'read')]
    public function show(Environment $twig, Notification $notification, NotificationRepository $notificationRepository): Response
    {
        $notification->setVue(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($notification);
        $em->flush();
        return new Response($twig->render('admin/notification/read.html.twig', [
            'notification' => $notification,
        ]));
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(Notification $notification): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($notification);
        $em->flush();

        return $this->redirectToRoute("admin_notification_received");
    }
}