<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Symfony\Component\HttpKernel\KernelInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $importUsers = Action::new('ImportUser', 'Importer des utilisateurs', 'fa fa-user')
            ->addCssClass('btn btn-primary')
            ->linkToRoute('importCsvUser');
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, $importUsers)
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        //$username = TextField::new('username');
        $password = TextField::new('password');
        $nom = TextField::new('nom');
        $prenom = TextField::new('prenom');
        $numAdh = NumberField::new('numAdh');
        $email = EmailField::new('email');
        $dateNaissance = DateField::new('dateNaissance');
        $lieuDeNaissance = TextField::new('lieuDeNaissance');
        $paysDeResidence = TextField::new('paysDeResidence');
        $departementDeResidence = TextField::new('departementDeResidence');
        $paysDeNaissance = TextField::new('paysDeNaissance');
        $ville = TextField::new('ville');
        $codePostale = TextField::new('codePostale');
        $profession = TextField::new('profession');
        $departementDeNaissance = TextField::new('departementDeNaissance');
        $tarification = TextField::new('tarification');
        $delegation = AssociationField::new('delegation');
        $roles = TextField::new('roles');
        if (Crud::PAGE_INDEX === $pageName) {
            return [ $nom, $prenom, $email, $ville, $roles];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$nom, $prenom, $numAdh, $email,
            $dateNaissance, $lieuDeNaissance, $paysDeResidence, $departementDeResidence,
            $departementDeNaissance, $paysDeNaissance, $ville, $codePostale,
            $profession, $tarification, $delegation, $roles];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$nom, $prenom, $numAdh, $email,
            $dateNaissance, $lieuDeNaissance, $paysDeResidence, $departementDeResidence,
            $departementDeNaissance, $paysDeNaissance, $ville, $codePostale,
            $profession, $tarification, $delegation, $roles];
        } elseif(Crud::PAGE_EDIT === $pageName) {
            return [$nom, $prenom, $numAdh, $email,
            $dateNaissance, $lieuDeNaissance, $paysDeResidence, $departementDeResidence,
            $departementDeNaissance, $paysDeNaissance, $ville, $codePostale,
            $profession, $tarification, $delegation, $roles];
        } else {
            return [ $nom, $prenom, $email, $ville, $roles];
        }
    }

    /**
     * @Route("/admin/user/import-csv-user", name="import-csv-user")
     */
    public function importCsvUser(KernelInterface $kernel): Response
    {
        /*$application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'app:create-user-from-csv2',
            // (optional) define the value of command arguments
            'fooArgument' => 'barValue',
            // (optional) pass options to the command
            '--message-limit' => $messages,
        ]);

        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();
        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();

        // return new Response(""), if you used NullOutput()
        return new Response($content);*/
        return $this->render('admin/importUser.html.twig', [
            'crudAction' => 'index'
        ]);
    }
}
