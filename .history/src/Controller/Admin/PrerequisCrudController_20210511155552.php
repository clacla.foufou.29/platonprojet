<?php

namespace App\Controller\Admin;

use App\Entity\Prerequis;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;


class PrerequisCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Prerequis::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $libellePrerequis = TextField::new('libellePrerequis');
        if (Crud::PAGE_INDEX === $pageName) {
            return [$libellePrerequis];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$libellePrerequis];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$libellePrerequis];
        } elseif(Crud::PAGE_EDIT === $pageName) {
            return [$libellePrerequis];
        } else {
            return [$libellePrerequis];
        }
    }
}
