<?php

namespace App\Controller\Admin;

use App\Entity\Prerogative;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PrerogativeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Prerogative::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $libellePrerogative = TextField::new('libellePrerogative');
        if (Crud::PAGE_INDEX === $pageName) {
            return [$libellePrerogative];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$libellePrerogative];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$libellePrerogative];
        } elseif(Crud::PAGE_EDIT === $pageName) {
            return [$libellePrerogative];
        } else {
            return [$libellePrerogative];
        }
    }

}
