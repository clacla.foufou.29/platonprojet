<?php

namespace App\Controller\Admin;

use App\Entity\Formation;
use App\Entity\Structure;
use App\Entity\Delegation;
use App\Entity\User;
use App\Entity\Prerequis;
use App\Entity\Prerogative;
use App\Entity\Categorie;
use App\Entity\Diplome;
use App\Entity\Cours;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Platon');
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::linkToDashboard('Accueil', 'fa fa-home'),

            MenuItem::section('Paramétrage'),
            MenuItem::linkToCrud('Formations', 'fa fa-tags', Formation::class),
            MenuItem::linkToCrud('Cours', 'fa fa-tags', Cours::class),
            MenuItem::linkToCrud('Structures', 'fa fa-file-text', Structure::class),
            MenuItem::linkToCrud('Diplomes', 'fa fa-tags', Diplome::class),
            MenuItem::linkToCrud('Categories', 'fa fa-tags', Categorie::class),
            MenuItem::linkToCrud('Prerequis', 'fa fa-tags', Prerequis::class),
            MenuItem::linkToCrud('Prerogatives', 'fa fa-tags', Prerogative::class),
            //MenuItem::linkToCrud('Cotisations', 'fa fa-tags', Cotisation::class),

            MenuItem::section('Delegation'),
            MenuItem::linkToCrud('Delegation', 'fa fa-comment', Delegation::class),
            MenuItem::linkToCrud('Adhérent', 'fa fa-user', User::class),
        ];
    }
}
