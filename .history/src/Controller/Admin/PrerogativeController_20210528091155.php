<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Prerogative;
use App\Form\PrerogativeType;
use App\Repository\PrerogativeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/prerogative", name="admin_prerogative_")
 * @package App\Controller\Admin
 */
class PrerogativeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(PrerogativeRepository $prerogative): Response
    {
        return $this->render('admin/prerogative/index.html.twig', [
            'prerogatives' => $prerogative->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutPrerogative(Request $request)
    {
        $prerogative = new Prerogative;

        //$form = $this->createForm(FormationType::class, $prerogative);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute('admin_prerogative_home');
        }

        return $this->render('admin/prerogative/ajout.html.twig', [
            'prerogative_form' => $form->createView()
        ]);
    }

    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Prerogative $prerogative, PrerogativeRepository $prerogativeRepository): Response
    {
        return new Response($twig->render('admin/prerogative/show.html.twig', [
            'prerogative' => $prerogative,
        ]));
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editPrerogative(Prerogative $prerogative, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(PrerogativeType::class, $prerogative);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($prerogative);
            $em->flush();

            return $this->redirectToRoute('admin_prerogative_home');
        }

        return $this->render('admin/prerogative/ajout.html.twig', [
            'form' => $form->createView(),
            'prerogative' => $prerogative
        ]);
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerPrerogative(Prerogative $prerogative)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($formation);
        $em->flush();

        $this->addFlash('message', 'Prerogative supprimé avec succès');
        return $this->redirectToRoute('prerogative');
    }
}