<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Categorie;
use App\Form\CategorieFormType;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/categorie", name="admin_categorie_")
 * @package App\Controller\Admin
 */
class CategorieController extends AbstractController
{

    #[Route('/', name: 'home')]
    public function index(CategorieRepository $categorieRepository): Response
    {
        return $this->render('admin/categorie/index.html.twig', [
            'categories' => $categorieRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ajout", name="ajout", requirements={"id":"\d+"})
     */
    public function ajoutCategorie(Request $request): Response
    {
        $categorie = new Categorie;

        $form = $this->createForm(CategorieFormType::class, $categorie);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();

            return $this->redirectToRoute('admin_categorie_home');
        }

        return $this->render('admin/categorie/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="show", requirements={"id":"\d+"})
     */
    public function show(Environment $twig, Categorie $categorie, CategorieRepository $categorieRepository): Response
    {
        return new Response($twig->render('admin/categorie/show.html.twig', [
            'categorie' => $categorie,
        ]));
    }

    /**
     * @Route("/modifier/{id}", name="modifier", requirements={"id":"\d+"})
     */
    public function ModifCategorie(Categorie $categorie, Request $request)
    {
        $form = $this->createForm(CategorieFormType::class, $categorie);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();

            return $this->redirectToRoute('admin_categorie_home');
        }

        return $this->render('admin/categorie/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/supprimer/{id}', name: 'supprimer')]
    public function supprimer(Categorie $categorie)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($categorie);
        $em->flush();

        $this->addFlash('message', 'Catégorie supprimée avec succès');
        return $this->redirectToRoute('admin_categorie_home');
    }
}
