<?php

namespace App\Controller\Admin;

use App\Entity\Structure;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;

class StructureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Structure::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $nomStructure = TextField::new('nomStructure');
        $raisonSociale = TextField::new('raisonSociale');
        $adresseStructure = TextField::new('adresseStructure');
        $villeStructure = TextField::new('villeStructure');
        $cpStructure = IntegerField::new('cpStructure');
        $mailStructure = EmailField::new('mailStructure');
        $telephoneStructure = TextField::new('telephoneStructure');
        if (Crud::PAGE_INDEX === $pageName) {
            return [$nomStructure, $raisonSociale, $adresseStructure, $villeStructure, $cpStructure, $mailStructure,$telephoneStructure];
        } elseif(Crud::PAGE_NEW === $pageName) {
            return [$nomStructure, $raisonSociale, $adresseStructure, $villeStructure, $cpStructure, $mailStructure,$telephoneStructure];
        } elseif(Crud::PAGE_DETAIL === $pageName) {
            return [$nomStructure, $raisonSociale, $adresseStructure, $villeStructure, $cpStructure, $mailStructure,$telephoneStructure];
        } elseif(Crud::PAGE_EDIT === $pageName) {
            return [$nomStructure, $raisonSociale, $adresseStructure, $villeStructure, $cpStructure, $mailStructure,$telephoneStructure];
        } else {
            return [$nomStructure, $raisonSociale, $adresseStructure, $villeStructure, $cpStructure, $mailStructure,$telephoneStructure];
        }
    }

}
