<?php

namespace App\Controller\Admin;

use DateTime;

use App\Entity\User;
use Twig\Environment;
use App\Form\UserType;
use App\Form\ImportUserType;
use App\Form\EditProfileType;
use App\Service\FileUploader;
use App\Entity\UploadImageUser;
use App\Repository\UserRepository;
use Symfony\Component\Console\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/user", name="admin_user_")
 * @package App\Controller\Admin
 */
class UserController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(UserRepository $user): Response
    {
        return $this->render('admin/user/index.html.twig', [
            'users' => $user->findAll(),
        ]);
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editUser(User $user, Request $request)
    {

        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
       
        if($form->isSubmitted() && $form->isValid()){
            
            // On récupère les images transmises
            if($form->get('UploadImageUser')->getData()!= null){

                //$delegation = $form->get('Delegation')->getData();
                $delegation = $this->getDoctrine()->getRepository(Delegation::class)->updateDeleguePerDelegation();

                $image = $form->get('UploadImageUser')->getData();
                
                    // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()).'.'.$image->guessExtension();
                    
                // On copie le fichier dans le dossier uploads
                $image->move($this->getParameter('avatar_directory'),$fichier);
                // On crée l'image dans la base de données
                $img = new UploadImageUser();
                $img->setName($fichier);
                $user->setUploadImageUser($img);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Profil mis à jour');
            return $this->redirectToRoute('admin_user_home');
        }

        return $this->render('admin/user/editProfile.html.twig', [
            'user_form' => $form->createView(),
            'user' => $user
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutUser(Request $request)
    {
        $user = new User;
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $user->setPassword("password");
            
            $image = $form->get('UploadImageUser')->getData();
            if($image != null){
                // On génère un nouveau nom de fichier
            $fichier = md5(uniqid()).'.'.$image->guessExtension();
                
            // On copie le fichier dans le dossier uploads
            $image->move($this->getParameter('avatar_directory'),$fichier);
            // On crée l'image dans la base de données
            $img = new UploadImageUser();
            $img->setName($fichier);
            $user->setUploadImageUser($img);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('admin_user_home');
        }

        return $this->render('admin/user/ajout.html.twig', [
            'user_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="show", requirements={"id":"\d+"})
     */
    public function show(Environment $twig, User $user, UserRepository $userRepository): Response
    /{
        return new Response($twig->render('admin/user/show.html.twig', [
            'user' => $user,
        ]));
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerUser(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('message', 'Utilisateur supprimé avec succès');
        return $this->redirectToRoute('admin_user_home');
    }

    #[Route('/suspendre/{id}', name:'suspendre')]
    public function suspendreUser(User $user)
    {
        $user->setIsActive(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $this->addFlash('message', 'Utilisateur suspendu avec succès');
        return $this->redirectToRoute('admin_user_home');
    }
    
    #[Route('/supprimer/image/{id}', name:'supprimer_image')]
    public function deleteImage(UploadImageUser $image, Request $request){
        
        $data = json_decode($request->getContent(), true);

        // On vérifie si le token est valide
        if($this->isCsrfTokenValid('delete'.$image->getId(), $data['_token'])){
            // On récupère le nom de l'image
            $nom = $image->getName();
            // On supprime le fichier
            unlink($this->getParameter('avatar_directory').'/'.$nom);

            // On supprime l'entrée de la base
            $em = $this->getDoctrine()->getManager();
            $em->remove($image);
            $em->flush();

            // On répond en json
            return new JsonResponse(['success' => 1]);
        }else{
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }

    #[Route('/import', name:'import')]
    public function importUser(Request $request, FileUploader $file_uploader)
    {
        $form = $this->createForm(ImportUserType::class);
        $form->handleRequest($request);
        $skip_row_number = array("1");

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $file = $form['upload_file']->getData();
            if ($file) 
            {
                $file_name = $file_uploader->upload($file);
                if (null !== $file_name) // for example
                {
                    $directory = $file_uploader->getTargetDirectory();
                    $full_path = $directory.'/'.$file_name;
                    // Do what you want with the full path file...
                    // Why not read the content or parse it !!!
                    $rowNo = 1;
                    // $fp is file pointer to file sample.csv
                    if (($fp = fopen($full_path, "r")) !== FALSE) {
                        while (($ligne = fgetcsv($fp, 1000, ",")) !== FALSE) {
                            if (in_array($ligne, $skip_row_number)){
                                continue;
                                // skip row of csv
                            } else {
                                $the_big_array[] = $ligne;
                            }
                        }
                        
                        fclose($fp);
                    }

                } else {
                // Oups, an error occured !!!
                }
            }

            $batchSize = 20;
            $em = $this->getDoctrine()->getManager();
            $userRepo = $this->getDoctrine()->getRepository(User::class);
            
            foreach($the_big_array as $i => $value){
                $newUser = new User();
                if( ! ( $newUser = $userRepo->findOneBy(['numAdh' => $value[0]]))) {
                    
                    $newUser->setNumAdh($value[0]);
                    $newUser->setNom($value[1]);
                    $newUser->setPrenom($value[2]);
                    $newUser->setEmail($value[3]);
                        //$date = date_create('j-M-Y', $userData['date_de_naissance']);
                        //$date = DateTime::createFromFormat('j-M-Y', $userData['date_de_naissance']);
                    $time = strtotime($value[4]);
                    $newformat = date('Y-m-d',$time);
                    $newUser->setDateNaissance(new DateTime($newformat));
                    $newUser->setLieuDeNaissance($value[5]);
                    $newUser->setPaysDeNaissance($value[6]);
                    $newUser->setPassword('password');
                    $em->persist($newUser);
                    if (($i % $batchSize) === 0) {
                        $em->flush();
                        $em->clear(); // Detaches all objects from Doctrine!
                    }
                    $em->clear();
                    continue;
                       
                }
                
                //$this->entityManager->persist($newUser);
                //dd($the_big_array);
            }
            $em->flush();
            $em->clear();
            return $this->redirectToRoute('admin_user_home');
        }
        return $this->render('admin/user/importUser.html.twig', [
            'import_user_form' => $form->createView(),
        ]);
    }

}