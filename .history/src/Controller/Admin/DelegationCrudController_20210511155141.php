<?php

namespace App\Controller\Admin;

use App\Entity\Delegation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DelegationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Delegation::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
