<?php

namespace App\Admin\Controller;

use Twig\Environment;
use App\Entity\Document;
use App\Form\DocumentType;
use App\Repository\DocumentRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/diplome", name="admin_diplome_")
 * @package App\Controller\Admin
 */
class DocumentController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(DocumentRepository $document): Response
    {
        return $this->render('admin/document/index.html.twig', [
            'documents' => $document->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutDocument(Request $request)
    {
        $document = new Document;

        $form = $this->createForm(DocumentType::class, $document);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ){
            // On récupère les images transmises
            $image = $form->get('nomDocument')->getData();
            $extension = $image->guessExtension();
            $fichier = md5(uniqid()).'.'.$extension;
            // On copie le fichier dans le dossier uploads
            $image->move($this->getParameter('document_directory'),$fichier);
            // On crée l'image dans la base de données
            $img = new Document();
            $img->setNomDocument($fichier);

            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();
            return $this->redirectToRoute('admin_document_home');
        }

        return $this->render('admin/document/ajout.html.twig', [
            'document_form' => $form->createView()
        ]);
    }

    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Document $document, DocumentRepository $documentRepository): Response
    {
        return new Response($twig->render('admin/document/show.html.twig', [
            'document' => $document,
        ]));
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editDocument(Document $document, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(DocumentType::class, $document);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $image = $form->get('nomDocument')->getData();
            $extension = $image->guessExtension();
            $fichier = md5(uniqid()).'.'.$extension;
                
            // On copie le fichier dans le dossier uploads
            $image->move($this->getParameter('document_directory'),$fichier);
            // On crée l'image dans la base de données
            $img = new Document();
            $img->setNomDocument($fichier);
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush();

            return $this->redirectToRoute('admin_document_home');
        }

        return $this->render('admin/document/edit.html.twig', [
            'document_form' => $form->createView(),
            'document' => $document
        ]);
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerDocument(Document $document)
    {
        $images = $document->getNomDocument();
        
        if($images){
            // On "génère" le chemin physique de l'image
            $nomImage = $this->getParameter("document_directory") . '/' . $images->getNom();
            // On vérifie si l'image existe
            if(file_exists($nomImage)){
                unlink($nomImage);

            }
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($document);
        $em->flush();

        $this->addFlash('message', 'Document supprimé avec succès');
        return $this->redirectToRoute('admin_document_home');
    }
}
