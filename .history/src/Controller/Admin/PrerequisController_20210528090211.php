<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Prerequis;
use App\Form\PrerequisType;
use App\Repository\PrerequisRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/prerequis", name="admin_prerequis_")
 * @package App\Controller\Admin
 */
class PrerequisController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(PrerequisRepository $prerequis): Response
    {
        return $this->render('admin/prerequis/index.html.twig', [
            'prerequis' => $prerequis->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutPrerequis(Request $request)
    {
        $prerequis = new Prerequis;

        $form = $this->createForm(PrerequisType::class, $formation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute('prerequis');
        }

        return $this->render('admin/prerequis/ajout.html.twig', [
            'prerequis_form' => $form->createView()
        ]);
    }

    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Prerequis $prerequis, PrerequisRepository $prerequisRepository): Response
    {
        return new Response($twig->render('admin/prerequis/show.html.twig', [
            'prerequis' => $prerequis,
        ]));
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editPrerequis(Prerequis $prerequis, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        //$form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($prerequis);
            $em->flush();

            return $this->redirectToRoute('user');
        }

        return $this->render('admin/prerequis/ajout.html.twig', [
            'form' => $form->createView(),
            'prerequis' => $prerequis
        ]);
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerPrerequis(Prerequis $prerequis)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($prerequis);
        $em->flush();

        $this->addFlash('message', 'Prerequis supprimé avec succès');
        return $this->redirectToRoute('prerequis');
    }
}