<?php

namespace App\Controller\Admin;

use App\Entity\Cours;
use Twig\Environment;
use App\Form\CoursType;
use App\Repository\CoursRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/delegation", name="admin_delegation_")
 * @package App\Controller\Admin
 */
class DelegationController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(DelegationRepository $delegation): Response
    {
        return $this->render('admin/delegation/index.html.twig', [
            'delegations' => $delegation->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutDelegation(Request $request)
    {
        $delegation = new Delegation;

        $form = $this->createForm(DelegationType::class, $delegation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($delegation);
            $em->flush();
            return $this->redirectToRoute('delegation');
        }

        return $this->render('admin/delegation/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }
    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Formation $formation, FormationRepository $formationRepository): Response
    {
        return new Response($twig->render('admin/formation/show.html.twig', [
            'formation' => $formation,
        ]));
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editCours(Delegation $delegation, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(DelegationType::class, $delegation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($delegation);
            $em->flush();

            return $this->redirectToRoute('user');
        }

        return $this->render('admin/delegation/ajout.html.twig', [
            'form' => $form->createView(),
            'delegation' => $delegation
        ]);
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerDelegation(Delegation $delegation)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($delegation);
        $em->flush();

        $this->addFlash('message', 'Délégation supprimé avec succès');
        return $this->redirectToRoute('delegation');
    }
}

