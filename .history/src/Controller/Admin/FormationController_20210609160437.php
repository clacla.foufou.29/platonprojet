<?php

namespace App\Controller\Admin;

use Twig\Environment;
use App\Entity\Formation;
use App\Form\FormationType;
use App\Repository\FormationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/formation", name="admin_formation_")
 * @package App\Controller\Admin
 */
class FormationController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(FormationRepository $formation): Response
    {
        return $this->render('admin/formation/index.html.twig', [
            'formations' => $formation->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutFormation(Request $request)
    {
        $formation = new Formation;

        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //$formation->setValidationFormation(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute('admin_formation_home');
        }

        return $this->render('admin/formation/ajout.html.twig', [
            'formation_form' => $form->createView()
        ]);
    }

    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Formation $formation, FormationRepository $formationRepository): Response
    {
        return new Response($twig->render('admin/formation/show.html.twig', [
            'formation' => $formation,
        ]));
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editFormation(Formation $formation, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();

            return $this->redirectToRoute('admin_formation_home');
        }

        return $this->render('admin/formation/ajout.html.twig', [
            'formation_form' => $form->createView(),
            'formation' => $formation
        ]);
    }

    #[Route('/valider/{id}', name:'valider')]
    public function valider(Formation $formation)
    {
        $formation->setValidationFormation(($formation->getValidationFormation())?false:true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($formation);
        $em->flush();

        return new Response("true");
    }

    #[Route('/supprimer/{id}', name:'supprimer')]
    public function supprimerFormation(Formation $formation)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($formation);
        $em->flush();

        $this->addFlash('message', 'Formation supprimée avec succès');
        return $this->redirectToRoute('admin_formation_home');
    }
}