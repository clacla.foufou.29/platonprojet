<?php

namespace App\Controller\Admin;

use App\Entity\Cours;
use Twig\Environment;
use App\Form\CoursType;
use App\Entity\Formation;
use App\Entity\Delegation;
use App\Form\DelegationType;
use App\Repository\CoursRepository;
use App\Repository\FormationRepository;
use App\Repository\DelegationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/delegation", name="admin_delegation_")
 * @package App\Controller\Admin
 */
class DelegationController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(DelegationRepository $delegation): Response
    {
        $delegations = $this->getDoctrine()
            ->getRepository(Delegation::class)
            ->countDeleguePerDelegation();
        var_dump($delegations);die;
        return $this->render('admin/delegation/index.html.twig', [
            'delegations' => $delegation->findAll(),
        ]);
    }

    #[Route('/ajout', name:'ajout')]
    public function ajoutDelegation(Request $request)
    {
        $delegation = new Delegation;

        $form = $this->createForm(DelegationType::class, $delegation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($delegation);
            $em->flush();
            return $this->redirectToRoute('admin_delegation_home');
        }

        return $this->render('admin/delegation/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }
    #[Route('/{id}', name: 'show')]
    public function show(Environment $twig, Delegation $delegation, DelegationRepository $delegationRepository): Response
    {
        return new Response($twig->render('admin/delegation/show.html.twig', [
            'delegation' => $delegation,
        ]));
    }

    #[Route('/edit/{id}', name:'edit')]
    public function editCours(Delegation $delegation, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(DelegationType::class, $delegation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($delegation);
            $em->flush();

            return $this->redirectToRoute('admin_delegation_home');
        }

        return $this->render('admin/delegation/ajout.html.twig', [
            'form' => $form->createView(),
            'delegation' => $delegation
        ]);
    }

}

