<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @IsGranted("ROLE_ADMIN")
 * @Route("/admin/diplome", name="admin_diplome_")
 * @package App\Controller\Admin
 */
class DocumentController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(DocumentRepository $document): Response
    {
        return $this->render('admin/document/index.html.twig', [
            'documents' => $document->findAll(),
        ]);
    }
}
