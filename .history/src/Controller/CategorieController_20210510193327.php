<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Form\CategorieFormType;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategorieController extends AbstractController
{
    #[Route('/categorie', name: 'categorie')]
    public function index(CategorieRepository $categorie): Response
    {
        return $this->render('categorie/index.html.twig', [
            'categorie' => $categorie->findAll(),
        ]);
    }
    #[Route('/categorie/ajout', name:'categorie_ajout')]
    public function ajoutCategorie(Request $request)
    {
        $categorie = new Categorie;

        $form = $this->createForm(CategorieFormType::class, $categorie);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();
            return $this->redirectToRoute('categorie');
        }

        return $this->render('categorie/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
