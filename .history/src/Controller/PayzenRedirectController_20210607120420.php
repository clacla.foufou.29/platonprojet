<?php

namespace App\Controller;


use DateTime;
use Monolog\Logger;
use App\Entity\Invoince;
use App\Entity\Cotisation;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Lyra\PayzenBundle\Payzen\PayzenService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Lyra\PayzenBundle\Includes\PayzenParamsUrl;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PayzenRedirectController extends AbstractController

{
        /**
         * @Route("/payzenRedirectForm/number3")
         */
        public function number()
        {
            $number = random_int(0, 100);

            return $this->render('payzenRedirectForm/number.html.twig', [
                'number' => $number,
            ]);
        }


        /**
         * @Route("/payzenRedirectForm/redirectPaymentLyraExample")
         */
        public function redirect_payment_lyra (PayzenService $payzen,ContainerInterface $container, Request $request)
        {

            

            $params= []; 

            $array_keys = $request->query->keys();
            foreach ($array_keys as &$key) {
                $params [$key] = $request->query->get($key);
            }

            $Form = $payzen->setRequestParams($params,$container);
        

            return $this->render ('payzenRedirectForm/formPayment.html.twig', [
                'payzen' => $Form
            ]);
        }
        
        

         /**
         * @Route("/payzenRedirectForm/renderForm")
         */
        public function create_form (PayzenService $payzen,ContainerInterface $container, Request $request)
        {

            $payzenParamsUrl = new PayzenParamsUrl();
            $payzenParamsUrl->setAmount("1000000");
            $payzenParamsUrl->setCaptureDelay("7");
            $payzenParamsUrl->setContrib("TestContrib");
            $payzenParamsUrl->setCurrency("170");
            $payzenParamsUrl->setCustAddress("TestAddress");
            $payzenParamsUrl->setCustCity("TestCity");
            $payzenParamsUrl->setCustCountry("FR");
            $payzenParamsUrl->setCustEmail("testemail@test.com");
            $payzenParamsUrl->setCustFirstName("TestName");
            $payzenParamsUrl->setCustId("1");
            $payzenParamsUrl->setCustLastName("TestLastName");
            $payzenParamsUrl->setCustPhone("33333333333333");
            $payzenParamsUrl->setCustZip("75000");
            $payzenParamsUrl->setLanguage("es");
            $payzenParamsUrl->setOrderId("820f1e8b7bde43fd95e18e3eac6e5644");
            $payzenParamsUrl->setReturnMode("GET");
            $payzenParamsUrl->setAmount("1000000");
            $payzenParamsUrl->setShipToCity("TestCity");
            $payzenParamsUrl->setShipToCountry("FR");
            $payzenParamsUrl->setShipToFirstName("TestName");
            $payzenParamsUrl->setShipToLastName("TestLastName");
            $payzenParamsUrl->setShipToStreet("TestStreet");
            $payzenParamsUrl->setShipToZip("75000");
            $payzenParamsUrl->setShippingAmount("1000");
            $payzenParamsUrl->setTaxRate("19");
            $payzenParamsUrl->setPageAction("PAYMENT");
            

            $form = $this->createFormBuilder($payzenParamsUrl)
            ->add('amount', TextType::class)
            ->add('capture_delay', HiddenType::class)
            ->add('contrib', TextType::class)
            ->add('currency', HiddenType::class)
            ->add('cust_address', TextType::class)
            ->add('cust_city', TextType::class)
            ->add('cust_country', TextType::class)
            ->add('cust_email', TextType::class)
            ->add('cust_last_name', TextType::class)
            ->add('cust_first_name', TextType::class)
            ->add('cust_phone', TextType::class)
            ->add('language', TextType::class)
            ->add('order_id', HiddenType::class)
            ->add('return_mode', HiddenType::class)
            ->add('cust_id', TextType::class)
            ->add('ship_to_city', TextType::class)
            ->add('ship_to_country', TextType::class)
            ->add('ship_to_first_name', TextType::class)
            ->add('ship_to_street', TextType::class)
            ->add('ship_to_zip', TextType::class)
            ->add('shipping_amount', TextType::class)
            ->add('tax_rate', TextType::class , ['label' => 'tax rate %'])
            ->add('page_action', HiddenType::class)
            ->add('Proced_checkout', SubmitType::class, ['label' => 'Checkout'])
            ->setAction($this->generateUrl('payment_lyra'))
            ->getForm();

            return $this->render('payzenRedirectForm/render.form.post.twig', [
                'form' => $form->createView(),
            ]);

        }





        /**
     * @Route("/invoice", name="create_invoice")
     */
    public function createInvoice(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invoice = new Cotisation();
        $invoice->setId($request->query->get("id"));
        $invoice->setTotalAmount($request->query->get("prixCotisation"));
        $entityManager->persist($invoice);
        $entityManager->flush();
        return new Response('Saved new invoice with id ');
    }

}
