<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Cours;
use App\Entity\Diplome;
use App\Form\CoursType;
use App\Entity\Formation;
use App\Form\DiplomeType;
use App\Form\FormationType;
use App\Form\EditProfileType;
use App\Entity\UploadImageUser;
use App\Entity\UploadImageDiplome;
use App\Repository\UserRepository;
use App\Form\FormateurDelegationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    #[Route('/user', name: 'user')]
    public function index(): Response
    {
        return $this->render('user/index.html.twig');
    }

    #[Route('/user/edit', name:'user_edit')]
    public function editUser(Request $request)
    {
        $user = $this->getUser();
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(EditProfileType::class, $user);

        $form->handleRequest($request);
       
        if($form->isSubmitted() && $form->isValid()){

            // On récupère les images transmises
            if($form->get('UploadImageUser')->getData()!= null){
                $image = $form->get('UploadImageUser')->getData();
                
                    // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()).'.'.$image->guessExtension();
                    
                // On copie le fichier dans le dossier uploads
                $image->move($this->getParameter('avatar_directory'),$fichier);
                // On crée l'image dans la base de données
                $img = new UploadImageUser();
                $img->setName($fichier);
                $user->setUploadImageUser($img);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Profil mis à jour');
            return $this->redirectToRoute('user');
        }

        return $this->render('user/editProfile.html.twig', [
            'editProfile_form' => $form->createView()
        ]);
    }

    #[Route('/user/add_delegation', name:'add_user_delegation')]
    public function addUserDelegation(UserRepository $userRepo, Request $request): Response
    {
        //$user = $this->getUser();
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        //$formateurs = $userRepo->findByFormateur('FORMATEUR').
        $form = $this->createForm(FormateurDelegationType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $formateur = $form->get('formateurs')->getData();
            $delegation = $this->getUser()->getDelegation();
            //$delegation = $this->$user->getDelegation();
            $delegation->setDelegue($delegation->getDelegue()+1);

            $formateur->setDelegation($delegation);

            $em = $this->getDoctrine()->getManager();
            $em->persist($formateur);
            $em->flush();

            $this->addFlash('message', 'Profil mis à jour');
            return $this->redirectToRoute('user');
        }
        return $this->render('user/ajoutFormateurDelegation.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
