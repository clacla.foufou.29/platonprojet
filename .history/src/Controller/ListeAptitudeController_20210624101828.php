<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListeAptitudeController extends AbstractController
{
    #[Route('/liste/aptitude', name: 'liste_aptitude')]
    public function index(DiplomeRepository $diplome): Response
    {
        return $this->render('liste_aptitude/index.html.twig', [
            'diplomes' => $diplome->findAll(),
        ]);
    }
    #[Route('/formation', name: 'formation')]
    public function index(FormationRepository $formation): Response
    {
        return $this->render('formation/index.html.twig', [
            'formations' => $formation->findAll(),
        ]);
    }
}
