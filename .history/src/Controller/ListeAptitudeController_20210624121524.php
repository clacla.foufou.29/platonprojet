<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\DiplomeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListeAptitudeController extends AbstractController
{
    #[Route('/liste/aptitude', name: 'liste_aptitude')]
    public function index(DiplomeRepository $diplome, UserRepository $user): Response
    {
        return $this->render('liste_aptitude/index.html.twig', [
            'diplomes' => $diplome->findAll(),
        ]);
    }
}
