<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use App\Entity\Cours;
use Twig\Environment;
use App\Form\CoursType;
use App\Entity\CoursSuivi;
use App\Form\PVStagiaireType;
use App\Entity\CoursStagiaire;
use App\Entity\CoursStagiaires;
use App\Form\ClotureStagiaireType;
use App\Repository\CoursRepository;
use App\Form\FormationStagiaireType;
use App\Form\ListeCoursStagiaireType;
use App\Repository\CoursStagiaireRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/cours", name="cours_")
 */
class CoursController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(CoursRepository $cours): Response
    {
        return $this->render('cours/index.html.twig', [
            'cours' => $cours->findAll(),
        ]);
    }

    /**
     * @Route("/cours/{id}")
     */
    #[Route('/cours/{id}', name: 'cours_show')]
    public function show(Environment $twig, Cours $cours, CoursRepository $coursRepository, Request $request): Response
    {
        $form = $this->createForm(FormationStagiaireType::class);

        $coursUser = new CoursSuivi;
        $coursStagiaire = new CoursStagiaires;

        $cours->addCoursSuivi($coursUser);
        $coursUser->setCours($cours);

        $cours->addCoursStagiaire($coursStagiaire);
        $coursStagiaire->setCours($cours);

        $form->handleRequest($request);
        //dd($cours);
        /*foreach($cours->getCoursSuivis() as $cour){
            dd($cour->getUser());
        }*/
       
        if($form->isSubmitted() && $form->isValid()){

            $stagiaires = $form->get('stagiaires')->getData();
            $adherents = $form->get('adherents')->getData();
            $em = $this->getDoctrine()->getManager();

            // On boucle sur les images
            foreach($stagiaires as $stagiaire){
                $coursStagiaire->setStagiaire($stagiaire);
                $stagiaire->addCoursStagiaire($coursStagiaire);
                $cours->addCoursStagiaire($coursStagiaire);
                $em->persist($stagiaire);
            
                $em->persist($coursStagiaire);
            }

            foreach($adherents as $adherent){
                $coursUser->setUser($adherent);
                $cours->addCoursSuivi($coursUser);
                $adherent->addCoursSuivi($coursUser);
                //$coursUser->setCours($cours);
                $em->persist($adherent);
            
                $em->persist($coursUser);
            }
            
            $em->persist($cours);
            $em->flush();
            $this->addFlash('message', 'Stagiaire ajouté');
        }

        return new Response($twig->render('cours/show.html.twig', [
            'cours' => $cours,
            'form' => $form->createView(),
        ]));
    }

    #[Route('/cours/ajout', name:'cours_ajout')]
    public function ajoutCours(Request $request)
    {
        $cours = new Cours;

        $form = $this->createForm(CoursType::class, $cours);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $users = $form->get('formateurs')->getData();
            foreach ($users as $user) {
                $user->addCour($cours);
            }
            $cours->setValidationCours(false);
            $cours->setEtat('Ouvert');
            $cours->setFormateurPrincipal($this->getUser());
            $this->getUser()->addCoursCree($cours);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cours);
            $em->flush();
            return $this->redirectToRoute('cours');
        }

        return $this->render('cours/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }


    #[Route('/cours/edit/{id}', name:'cours_edit')]
    public function editCours(Cours $cours, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(CoursType::class, $cours);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($cours);
            $em->flush();

            return $this->redirectToRoute('cours');
        }

        return $this->render('cours/ajout.html.twig', [
            'form' => $form->createView(),
            'cours' => $cours
        ]);
    }

    #[Route('/cours/supprimer/{id}', name:'cours_supprimer')]
    public function supprimerCours(Cours $cours)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($cours);
        $em->flush();

        $this->addFlash('message', 'Cours supprimé avec succès');
        return $this->redirectToRoute('cours');
    }

    #[Route('/cours/{id}/cloture', name:'cours_cloture')]
    public function clotureCours(Request $request, Cours $cours)
    {

        $repository = $this->getDoctrine()->getRepository(CoursStagiaires::class);
        $repositoryUser = $this->getDoctrine()->getRepository(CoursSuivi::class);
        
        $listeCoursStagiaire = $repository->findBy(['cours' => $cours]);
        $listeCoursSuivi = $repositoryUser->findBy(['cours' => $cours]);

        //$coursStagiaire->setCours($cours);
        $formS = $this->createForm(ListeCoursStagiaireType::class,
            [
                'listeStagiaire' => $listeCoursStagiaire
            ]
        );
        $formA = $this->createForm(ListeCoursSuiviType::class,
            [
                'listeAdherent' => $listeCoursSuivi
            ]
        );
        $formS->handleRequest($request);
        $formA->handleRequest($request);
        //dd($listeCoursStagiaire);
        if($formS->isSubmitted() && $formS->isValid() && $formA->isSubmitted() && $formA->isValid()){
            //dd($form);
            $em = $this->getDoctrine()->getManager();
            foreach($listeCoursStagiaire as $liste){
                $em->persist($liste);
            }
            
            $em->flush();

            return $this->redirectToRoute('admin_cours_home');
        }

        return $this->render('admin/cours/cloture.html.twig', [
            'formS' => $formS->createView(),
            'formA' => $formA->createView(),
            'listeCoursStagiaire' => $listeCoursStagiaire,
            'listeCoursSuivi' => $listeCoursSuivi,
        ]);
       
    }

    #[Route('/cours/{id}/genere', name: 'cours_cloture_pv')]
    public function genererPV(Cours $cours)
    {
        // On définit les options du PDF
        $pdfOptions = new Options();
        // Police par défaut
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        // On instancie Dompdf
        $dompdf = new Dompdf($pdfOptions);
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $dompdf->setHttpContext($context);

        $repository = $this->getDoctrine()->getRepository(CoursStagiaires::class);
        $repositoryUser = $this->getDoctrine()->getRepository(CoursSuivi::class);

        $listeCoursStagiaire = $repository->findBy(['cours' => $cours]);
        $listeCoursSuivi = $repositoryUser->findBy(['cours' => $cours]);

        //dd($listeCoursStagiaire);
        // On génère le html
        $html = $this->renderView('cours/PV.html.twig', [
            'listeCoursStagiaire' => $listeCoursStagiaire,
            'listeCoursSuivi' => $listeCoursSuivi,
            'cours' => $cours,
        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
 
        // On génère un nom de fichier
        $fichier = 'cours-'. $cours->getNomCours() .'.pdf';
 
        // On envoie le PDF au navigateur
        $dompdf->stream($fichier, [
            'Attachment' => true
        ]);
 
        return new Response();
    }
}
