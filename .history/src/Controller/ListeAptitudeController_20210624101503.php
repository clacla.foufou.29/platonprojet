<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListeAptitudeController extends AbstractController
{
    #[Route('/liste/aptitude', name: 'liste_aptitude')]
    public function index(): Response
    {
        return $this->render('liste_aptitude/index.html.twig', [
            'controller_name' => 'ListeAptitudeController',
        ]);
    }
}
