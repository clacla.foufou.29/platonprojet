<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Delegation;
use App\Form\DelegationType;
use App\Repository\DelegationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DelegationController extends AbstractController
{
    #[Route('/delegation', name: 'delegation')]
    public function index(DelegationRepository $delegation): Response
    {
        dd($delegation);
        return $this->render('delegation/index.html.twig', [
            'delegations' => $delegation->findAll(),
        ]);
    }

    #[Route('/delegation/ajout', name:'delegation_ajout')]
    public function ajoutDelegation(Request $request)
    {
        $delegation = new Delegation;

        $form = $this->createForm(DelegationType::class, $delegation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $delegation->setDelegue(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($delegation);
            $em->flush();
            return $this->redirectToRoute('delegation');
        }

        return $this->render('delegation/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/delegation/ajoutFormateur', name:'delegation_ajout_formateur')]
    public function ajoutFormateur(Request $request)
    {
        $delegation = $this->getUser()->getDelegation();
        $form = $this->createForm(DelegationType::class, $delegation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($delegation);
            $em->flush();
            return $this->redirectToRoute('delegation');
        }

        return $this->render('delegation/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    #[Route('/delegation/{id}', name: 'delegation_show')]
    public function show(Environment $twig, Formation $formation, FormationRepository $formationRepository): Response
    {
        return new Response($twig->render('formation/show.html.twig', [
            'formation' => $formation,
        ]));
    }

    #[Route('/delegation/supprimer/{id}', name:'supprimer_adherent')]
    public function supprimerAdherent(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash('message', 'Adhérent supprimé avec succès');
        return $this->redirectToRoute('delegation');
    }
}
