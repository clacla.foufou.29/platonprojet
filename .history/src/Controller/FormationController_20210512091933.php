<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Form\FormationType;
use App\Repository\FormationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FormationController extends AbstractController
{
    #[Route('/formation', name: 'formation')]
    public function index(FormationRepository $formation): Response
    {
        return $this->render('formation/index.html.twig', [
            'formations' => $formation->findAll(),
        ]);
    }

    #[Route('/formation/ajout', name:'formation_ajout')]
    public function ajoutFormation(Request $request)
    {
        $formation = new Formation;

        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute('formation');
        }

        return $this->render('formation/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/formation/edit/{id}', name:'formation_edit')]
    public function editFormation(Formation $formation, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();

            return $this->redirectToRoute('user');
        }

        return $this->render('formation/ajout.html.twig', [
            'form' => $form->createView(),
            'formation' => $formation
        ]);
    }

    #[Route('/formation/supprimer/{id}', name:'formation_supprimer')]
    public function supprimerFormation(Formation $formation)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($formation);
        $em->flush();

        $this->addFlash('message', 'Formation supprimé avec succès');
        return $this->redirectToRoute('formation');
    }
}
