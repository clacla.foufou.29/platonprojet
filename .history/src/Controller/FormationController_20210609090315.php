<?php

namespace App\Controller;

use Twig\Environment;
use App\Entity\Formation;
use App\Form\FormationType;
use App\Form\FormationDocumentType;
use App\Form\FormationStagiaireType;
use App\Entity\UploadDocumentFormation;
use App\Repository\FormationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FormationController extends AbstractController
{
    #[Route('/formation', name: 'formation')]
    public function index(FormationRepository $formation): Response
    {
        return $this->render('formation/index.html.twig', [
            'formations' => $formation->findAll(),
        ]);
    }

    #[Route('/formation/ajout', name:'formation_ajout')]
    public function ajoutFormation(Request $request)
    {
        $formation = new Formation;

        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $formation->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute('formation');
        }

        return $this->render('formation/ajout.html.twig', [
            'formation_form' => $form->createView()
        ]);
    }

    #[Route('/formation/{id}', name: 'formation_show')]
    public function show(Environment $twig, Formation $formation, FormationRepository $formationRepository, Request $request): Response
    {
        $form = $this->createForm(FormationStagiaireType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $stagiaires = $form->get('stagiaire')->getData();

            // On boucle sur les images
            foreach($stagiaires as $stagiaire){
                $stagiaire->addFormation($formation);
                $formation->addStagiaire($stagiaire);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($stagiaire);
            $em->flush();
            $this->addFlash('message', 'Stagiaire ajouté');
        }

        return $this->render('formateur_stagiaire/ajoutList.html.twig', [
            'form' => $form->createView()
        ]);
    }
        return new Response($twig->render('formation/show.html.twig', [
            'formation' => $formation,
        ]));
    }

    #[Route('/formation/edit/{id}', name:'formation_edit')]
    public function editFormation(Formation $formation, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            //$annonce->setActive(false);
            // On récupère les images transmises
            //$images = $form->get('images')->getData();
                
            // On ajoute les images
            //$picturesService->add($images, $annonce);

            $em = $this->getDoctrine()->getManager();
            $em->persist($formation);
            $em->flush();

            return $this->redirectToRoute('formation');
        }

        return $this->render('formation/ajout.html.twig', [
            'form' => $form->createView(),
            'formation' => $formation
        ]);
    }

    #[Route('/formation/editDocument/{id}', name:'formation_edit_document')]
    public function ajoutDocumentFormation(Formation $formation, Request $request)
    {
        //$this->denyAccessUnlessGranted('annonce_edit', $annonce);
        $form = $this->createForm(FormationDocumentType::class, $formation);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $images = $form->get('uploadDocumentFormation')->getData();

            // On boucle sur les images
            foreach($images as $image){
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                // On copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('formation_directory'),
                    $fichier
                );

                // On stocke l'image dans la base de données (son nom)
                $img = new UploadDocumentFormation();
                $img->setNom($fichier);
                $formation->addUploadDocumentFormation($img);
            }

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('formation');
        }

        return $this->render('formation/ajoutDocument.html.twig', [
            'formation_form' => $form->createView(),
            'formation' => $formation
        ]);
    }

    #[Route('/formation/supprimer/{id}', name:'formation_supprimer')]
    public function supprimerFormation(Formation $formation)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($formation);
        $em->flush();

        $this->addFlash('message', 'Formation supprimé avec succès');
        return $this->redirectToRoute('formation');
    }
}
