<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('roles')
            ->add('password')
            ->add('isVerified')
            ->add('nom')
            ->add('prenom')
            ->add('dateNaissance')
            ->add('numAdh')
            ->add('lieuDeNaissance')
            ->add('paysDeResidence')
            ->add('departementDeResidence')
            ->add('paysDeNaissance')
            ->add('adressePhysique')
            ->add('ville')
            ->add('codePostale')
            ->add('profession')
            ->add('departementDeNaissance')
            ->add('tarification')
            ->add('delegation')
            ->add('cours')
            ->add('UploadImageUser')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
