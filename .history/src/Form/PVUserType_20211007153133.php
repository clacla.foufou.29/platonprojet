<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\CoursSuivi;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PVUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                // looks for choices from this entity
                'class' => User::class,
                'disabled' => true,
                'label' => null,
            
                // uses the User.username property as the visible option string
                //'choice_label' => 'Adherent',
            
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('apte', CheckboxType::class, [
                'required'   => false,
            ])
            ->add('present', CheckboxType::class, [
                'required'   => false,
            ])
            ->add('motif', TextType::class, [
                'required'   => false,
            ])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CoursSuivi::class,
        ]);
    }
}
