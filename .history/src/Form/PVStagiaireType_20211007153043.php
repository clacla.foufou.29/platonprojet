<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Stagiaire;
use App\Entity\CoursStagiaires;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class PVStagiaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
            ->add('stagiaire', EntityType::class, [
                // looks for choices from this entity
                'class' => Stagiaire::class,
                'disabled' => true,
                'label' => false,
            
                // uses the User.username property as the visible option string
                //'choice_label' => 'username',
            
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('apte', CheckboxType::class, [
                'required'   => false,
            ])
            ->add('present', CheckboxType::class, [
                'required'   => false,
                'label' => 'Présent'
            ])
            ->add('motif', TextType::class, [
                'required'   => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CoursStagiaires::class,
        ]);
    }
}
