<?php

namespace App\Form;

use App\Entity\Structure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StructureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomStructure', TextType::class)
            ->add('adresseStructure', TextType::class)
            ->add('cpStructure', TextType::class)
            ->add('villeStructure', TextType::class)
            ->add('raisonSociale', TextType::class)
            ->add('mailStructure', MailType::class)
            ->add('telephoneStructure', TextType::class)
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Structure::class,
        ]);
    }
}
