<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class EditProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('dateNaissance', DateType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',]
            
            )
            ->add('lieuDeNaissance', TextType::class)
            ->add('paysDeResidence', TextType::class)
            ->add('departementDeResidence', EntityType::class, [
                    'required' => false,
                    'class' => Departement::class,
                    'choice_label' => function(Departement $departement) {
                        return sprintf('%s', $departement->getNomDepartement());
                    },
                    'placeholder' => 'Choisir un departement'
            ])
            ->add('paysDeNaissance', TextType::class)
            ->add('adressePhysique', TextType::class)
            ->add('ville', TextType::class)
            ->add('codePostale', TextType::class)
            ->add('profession', TextType::class)
            ->add('departementDeNaissance', TextType::class)
            ->add('uploadImageUser', FileType::class,[
                'label' => false,
                'multiple' => false,
                'mapped' => false,
                'required' => false
            ])
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
