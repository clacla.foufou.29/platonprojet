<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Delegation;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('roles', TextType::class)
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('dateNaissance', BirthdayType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',
            ])
            ->add('numAdh', IntegerType::class)
            ->add('lieuDeNaissance', TextType::class)
            ->add('paysDeResidence', TextType::class, [
                'required' => false,
            ])
            ->add('departementDeResidence', EntityType::class, [
                    'required' => false,
                    'class' => Delegation::class,
                    'choice_label' => function(Delegation $delegation) {
                        return sprintf('%s', $delegation->getNomDelegation());
                    },
                    'placeholder' => 'Choisir une délégation'
                ]
            ])
            ->add('paysDeNaissance', TextType::class)
            ->add('adressePhysique', TextType::class, [
                'required' => false,
            ])
            ->add('ville', TextType::class, [
                'required' => false,
            ])
            ->add('codePostale',TextType::class, [
                'required' => false,
            ])
            ->add('profession', TextType::class, [
                'required' => false,
            ])
            ->add('departementDeNaissance', TextType::class, [
                'required' => false,
            ])
            ->add('tarification', TextType::class, [
                'required' => false,
            ])
            ->add('delegation', EntityType::class, [
                'required' => false,
                'class' => Delegation::class,
                'choice_label' => function(Delegation $delegation) {
                    return sprintf('%s', $delegation->getNomDelegation());
                },
                'placeholder' => 'Choisir une délégation'
            ])
            ->add('UploadImageUser', FileType::class,[
                'label' => 'avatar',
                'multiple' => false,
                'mapped' => false,
                'required' => false
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Adherent' => 'ROLE_ADHERENT',
                    'Formateur' => 'ROLE_FORMATEUR',
                    'Delegue' => 'ROLE_DELEGUE',
                    'Administrateur' => 'ROLE_ADMIN'
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles'
            ])
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
