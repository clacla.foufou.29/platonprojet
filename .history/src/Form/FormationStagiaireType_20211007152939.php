<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Stagiaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormationStagiaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('stagiaires', EntityType::class, [
            'class' => Stagiaire::class,
            'mapped' => false,
            'required' => false,
            'multiple' => true,
            'placeholder' => 'Choisir un ou des stagiaires',
            'label' => 'stagiaires'
            ])
        ->add('adherents', EntityType::class, [
            'class' => User::class,
            'mapped' => false,
            'required' => false,
            'multiple' => true,
            'placeholder' => 'Choisir un ou des adhérents',
            'label' => 'adhérents'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stagiaire::class,
        ]);
    }
}
