<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Cours;
use App\Entity\Formation;
use App\Entity\Structure;
use App\Entity\Delegation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class CoursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomCours', TextType::class)
            ->add('adresse', TextType::class)
            ->add('cp', IntegerType::class)
            ->add('ville', TextType::class)
            ->add('dateDebut', DateType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',])
            ->add('dateFin', DateType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',])
            ->add('nbStagiaire', IntegerType::class)
            //->add('validationCours', CheckboxType::class)
            
            ->add('formation', EntityType::class, [
                'class' => Formation::class],)
            ->add('structure', EntityType::class, [
                'class' => Structure::class],)
            ->add('delegation', EntityType::class, [
                'class' => Delegation::class],)
            ->add('formateurs', EntityType::class, [
                'class' => User::class,
                'multiple' => true],
                )
            ->add('valider', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-large btn-default btn-block',
                ],
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cours::class,
        ]);
    }
}
