<?php

namespace App\Form;

use App\Entity\Delegation;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormateurDelegationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('formateurs', EntityType::class, [
            'class' => User::class,
            'multiple' => true,
            'query_builder' => function (UserRepository $er) {
                return $er->createQueryBuilder('u')
                ->orderBy('u.roles', 'ASC')
                ->where('u.roles LIKE :role')
                ->setParameter('role', '%"'.'ROLE_FORMATEUR'.'"%');
                },
            ])
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Delegation::class,
        ]);
    }
}
