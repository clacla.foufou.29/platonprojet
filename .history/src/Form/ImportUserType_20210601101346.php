<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Delegation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ImportUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('delegation', EntityType::class, [
                'class' => Delegation::class,
                'choice_label' => function(Delegation $delegation) {
                    return sprintf('%s', $delegation->getNomDelegation());
                },
                'placeholder' => 'Choisir une délégation'
                ])*/
                ->add('fileCSV', FileType::class,[
                    'label' => false,
                    'multiple' => false,
                    'mapped' => false,
                    'required' => false
                ])
            ->add('valider', SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-primary"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
