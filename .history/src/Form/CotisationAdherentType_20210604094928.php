<?php

namespace App\Form;

use App\Entity\Cotisation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CotisationAdherentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('libelleCotisation', HiddenType::class)
        ->add('typeCotisation',  HiddenType::class)
        ->add('anneeCotisation',  HiddenType::class)
        ->add('prixCotisation', HiddenType::class)
            ->add('moyenDePaiement', ChoiceType::class, [
                'choices'  => [
                    'Chèque' => 'Chèque',
                    'CB' => 'CB',
                ]])
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cotisation::class,
        ]);
    }
}
