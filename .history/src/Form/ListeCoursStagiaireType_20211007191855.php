<?php

namespace App\Form;

use App\Form\PVStagiaireType;
use App\Entity\CoursStagiaires;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ListeCoursStagiaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('listeStagiaire', CollectionType::class, [
                'entry_type' => PVStagiaireType::class
            ])
        ;
    }

}
