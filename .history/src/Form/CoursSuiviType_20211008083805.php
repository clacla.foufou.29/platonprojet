<?php

namespace App\Form;

use App\Entity\CoursSuivi;
use Symfony\Component\Form\AbstractType;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CoursSuiviType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apte', Boolean::class)
            ->add('present', Boolean::class)
            ->add('motif', TextType::class)
            ->add('cours')
            ->add('stagiaire')
            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CoursSuivi::class,
        ]);
    }
}
