<?php

namespace App\Form;

use App\Entity\Delegation;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormateurDelegationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('formateurs', EntityType::class, [
            'class' => User::class,
            'multiple' => true,
            'query_builder' => function(UserRepository $repository) {
                $qb = $repository->createQueryBuilder('u');
                // the function returns a QueryBuilder object
                return $qb
                    // find all users where 'deleted' is NOT '1'
                    ->where('JSON_CONTAINS(u.roles, :role) = 1')
                    ->setParameter('role', '"ROLE_' . $role . '"')
                ;
            }
            ])
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Delegation::class,
        ]);
    }
}
