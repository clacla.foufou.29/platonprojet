<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Diplome;
use App\Entity\Categorie;
use App\Entity\Formation;
use App\Entity\Prerequis;
use App\Form\DiplomeType;
use App\Entity\Prerogative;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FormationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libelleFormation', TextType::class)
            ->add('age', IntegerType::class)
            ->add('nbMinStagiaire', IntegerType::class)
            ->add('nbMaxStagiaire', IntegerType::class)
            ->add('nbFormateur', IntegerType::class)
            ->add('prixFormation', IntegerType::class)
            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => function(Categorie $categorie) {
                    return sprintf('%s', $categorie->getNomCategorie());
                },
                'placeholder' => 'Choisir une catégorie'
                ])
            ->add('prerogatives', EntityType::class, [
                'class' => Prerogative::class,
                'multiple' => true,
                'required' => false,
                'placeholder' => 'Choisir une ou des prerogatives'
            ],)
            ->add('prerequis', EntityType::class, [
                'class' => Formation::class,
                'multiple' => true,
                'required' => false,
                'choice_label' => 'libelleFormation',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.libelleFormation', 'ASC');
                },
                'by_reference' => false,
                'attr' => [
                    'class' => 'select-prerequis'
                ]
            ],)
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formation::class,
        ]);
    }
}
