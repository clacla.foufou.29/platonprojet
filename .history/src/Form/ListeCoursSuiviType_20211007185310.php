<?php

namespace App\Form;

use App\Form\PVUserType;
use App\Entity\CoursSuivi;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ListeCoursSuiviType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('listeAdherent', CollectionType::class, [
                'entry_type' => PVUserType::class
            ])
            
        ;
    }

}
