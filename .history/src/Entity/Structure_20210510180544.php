<?php

namespace App\Entity;

use App\Repository\StructureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StructureRepository::class)
 */
class Structure
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomStructure;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresseStructure;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cpStructure;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $villeStructure;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $raisonSociale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mailStructure;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephoneStructure;

    /**
     * @ORM\OneToOne(targetEntity=Cours::class, mappedBy="structure")
     */
    private $cours;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomStructure(): ?string
    {
        return $this->nomStructure;
    }

    public function setNomStructure(string $nomStructure): self
    {
        $this->nomStructure = $nomStructure;

        return $this;
    }

    public function getAdresseStructure(): ?string
    {
        return $this->adresseStructure;
    }

    public function setAdresseStructure(string $adresseStructure): self
    {
        $this->adresseStructure = $adresseStructure;

        return $this;
    }

    public function getCpStructure(): ?string
    {
        return $this->cpStructure;
    }

    public function setCpStructure(string $cpStructure): self
    {
        $this->cpStructure = $cpStructure;

        return $this;
    }

    public function getVilleStructure(): ?string
    {
        return $this->villeStructure;
    }

    public function setVilleStructure(string $villeStructure): self
    {
        $this->villeStructure = $villeStructure;

        return $this;
    }

    public function getRaisonSociale(): ?string
    {
        return $this->raisonSociale;
    }

    public function setRaisonSociale(string $raisonSociale): self
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    public function getMailStructure(): ?string
    {
        return $this->mailStructure;
    }

    public function setMailStructure(string $mailStructure): self
    {
        $this->mailStructure = $mailStructure;

        return $this;
    }

    public function getTelephoneStructure(): ?string
    {
        return $this->telephoneStructure;
    }

    public function setTelephoneStructure(string $telephoneStructure): self
    {
        $this->telephoneStructure = $telephoneStructure;

        return $this;
    }
}
