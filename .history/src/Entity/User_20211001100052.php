<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(
     *     message = "L' email '{{ value }}' n'est pas valide."
     * )
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, unique=false, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="date", length=500)
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $numAdh;

    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=true)
     */
    private $lieuDeNaissance;

    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=true)
     */
    private $paysDeResidence;

    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=true)
     */
    private $paysDeNaissance;

    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=true)
     */
    private $adressePhysique;

    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", unique=false, nullable=true)
     * @Assert\Length(max="5", min="5", minMessage="Code postal invalide", maxMessage="Code postal invalide")
     * @Assert\Regex("/[0-9]{2}[0-9]{3}/", message="Code postal invalide")
     */
    private $codePostale;

    /**
     * @ORM\Column(type="string", length=500, unique=false, nullable=true)
     */
    private $profession;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $tarification;

    /**
     * @ORM\OneToMany(targetEntity=Diplome::class, mappedBy="user")
     */
    private $diplomes;

    /**
     * @ORM\ManyToOne(targetEntity=Delegation::class, inversedBy="formateurs")
     */
    private $delegation;

    /**
     * @ORM\ManyToMany(targetEntity=Cours::class, inversedBy="formateurs")
     */
    private $cours;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="formateurPrincipal")
     */
    private $coursCrees;

    /**
     * @ORM\OneToMany(targetEntity=Cotisation::class, mappedBy="users")
     */
    private $cotisations;

    /**
     * @ORM\OneToOne(targetEntity=UploadImageUser::class, inversedBy="user", cascade={"persist", "remove"})
     */
    private $UploadImageUser;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="sender", orphanRemoval=true)
     */
    private $sent;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="recipient", orphanRemoval=true)
     */
    private $received;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=Formation::class, mappedBy="user")
     */
    private $formation;

    /**
     * @ORM\OneToMany(targetEntity=CoursSuivi::class, mappedBy="user")
     */
    private $coursSuivis;

    /**
     * @ORM\ManyToOne(targetEntity=Departement::class, inversedBy="users")
     */
    private $DepartementDeResidence;

    public function __construct()
    {
        $this->isActive = true;
        //$this->username = $username;
        //$this->email = $email;
        $this->diplomes = new ArrayCollection();
        //$this->delegation = new ArrayCollection();
        $this->cours = new ArrayCollection();
        $this->coursCrees = new ArrayCollection();
        $this->sent = new ArrayCollection();
        $this->received = new ArrayCollection();
        $this->formation = new ArrayCollection();
        $this->cotisations = new ArrayCollection();
        $this->coursSuivis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /*
     * Get isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
 
    /*
     * Set isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getNumAdh(): ?string
    {
        return $this->numAdh;
    }

    public function setNumAdh(string $numAdh): self
    {
        $this->numAdh = $numAdh;

        return $this;
    }

    public function getLieuDeNaissance(): ?string
    {
        return $this->lieuDeNaissance;
    }

    public function setLieuDeNaissance(?string $lieuDeNaissance): self
    {
        $this->lieuDeNaissance = $lieuDeNaissance;

        return $this;
    }

    public function getPaysDeResidence(): ?string
    {
        return $this->paysDeResidence;
    }

    public function setPaysDeResidence(?string $paysDeResidence): self
    {
        $this->paysDeResidence = $paysDeResidence;

        return $this;
    }

    public function getDepartementDeResidence(): ?string
    {
        return $this->DepartementDeResidence;
    }

    public function setDepartementDeResidence(?string $departementDeResidence): self
    {
        $this->DepartementDeResidence = $departementDeResidence;

        return $this;
    }

    public function getPaysDeNaissance(): ?string
    {
        return $this->paysDeNaissance;
    }

    public function setPaysDeNaissance(?string $paysDeNaissance): self
    {
        $this->paysDeNaissance = $paysDeNaissance;

        return $this;
    }

    public function getAdressePhysique(): ?string
    {
        return $this->adressePhysique;
    }

    public function setAdressePhysique(?string $adressePhysique): self
    {
        $this->adressePhysique = $adressePhysique;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostale(): ?string
    {
        return $this->codePostale;
    }

    public function setCodePostale(?string $codePostale): self
    {
        $this->codePostale = $codePostale;

        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setProfession(?string $profession): self
    {
        $this->profession = $profession;

        return $this;
    }


    public function getTarification(): ?string
    {
        return $this->tarification;
    }

    public function setTarification(string $tarification): self
    {
        $this->tarification = $tarification;

        return $this;
    }

    /**
     * @return Collection|Diplome[]
     */
    public function getDiplomes(): Collection
    {
        return $this->diplomes;
    }

    public function addDiplome(Diplome $diplome): self
    {
        if (!$this->diplomes->contains($diplome)) {
            $this->diplomes[] = $diplome;
            $diplome->setUser($this);
        }

        return $this;
    }

    public function removeDiplome(Diplome $diplome): self
    {
        if ($this->diplomes->removeElement($diplome)) {
            // set the owning side to null (unless already changed)
            if ($diplome->getUser() === $this) {
                $diplome->setUser(null);
            }
        }

        return $this;
    }

    public function getDelegation(): ?Delegation
    {
        return $this->delegation;
    }

    public function setDelegation(?Delegation $delegation): self
    {
        $this->delegation = $delegation;

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        $this->cours->removeElement($cour);

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCoursCrees(): Collection
    {
        return $this->coursCrees;
    }

    public function addCoursCree(Cours $coursCree): self
    {
        if (!$this->coursCrees->contains($coursCree)) {
            $this->coursCrees[] = $coursCree;
            $coursCree->setFormateurPrincipal($this);
        }

        return $this;
    }

    public function removeCoursCree(Cours $coursCree): self
    {
        if ($this->coursCrees->removeElement($coursCree)) {
            // set the owning side to null (unless already changed)
            if ($coursCree->getFormateurPrincipal() === $this) {
                $coursCree->setFormateurPrincipal(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getNom().' '.$this->getPrenom() ;
    }

    public function getUploadImageUser(): ?UploadImageUser
    {
        return $this->UploadImageUser;
    }

    public function setUploadImageUser(?UploadImageUser $UploadImageUser): self
    {
        $this->UploadImageUser = $UploadImageUser;

        return $this;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setSender($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getSender() === $this) {
                $notification->setSender(null);
            }
        }

        return $this;
    }
     /**
     * @return Collection|Notification[]
     */
    public function getSent(): Collection
    {
        return $this->sent;
    }

    public function addSent(Notification $sent): self
    {
        if (!$this->sent->contains($sent)) {
            $this->sent[] = $sent;
            $sent->setSender($this);
        }

        return $this;
    }

    public function removeSent(Notification $sent): self
    {
        if ($this->sent->removeElement($sent)) {
            // set the owning side to null (unless already changed)
            if ($sent->getSender() === $this) {
                $sent->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getReceived(): Collection
    {
        return $this->received;
    }

    public function addReceived(Notification $received): self
    {
        if (!$this->received->contains($received)) {
            $this->received[] = $received;
            $received->setRecipient($this);
        }

        return $this;
    }

    public function removeReceived(Notification $received): self
    {
        if ($this->received->removeElement($received)) {
            // set the owning side to null (unless already changed)
            if ($received->getRecipient() === $this) {
                $received->setRecipient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormation(): Collection
    {
        return $this->formation;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formation->contains($formation)) {
            $this->formation[] = $formation;
            $formation->setUser($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formation->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getUser() === $this) {
                $formation->setUser(null);
            }
        }

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    /**
     * @return Collection|Cotisation[]
     */
    public function getCotisations(): Collection
    {
        return $this->cotisations;
    }

    public function addCotisation(Cotisation $cotisation): self
    {
        if (!$this->cotisations->contains($cotisation)) {
            $this->cotisations[] = $cotisation;
            $cotisation->setUsers($this);
        }

        return $this;
    }

    public function removeCotisation(Cotisation $cotisation): self
    {
        if ($this->cotisations->removeElement($cotisation)) {
            // set the owning side to null (unless already changed)
            if ($cotisation->getUsers() === $this) {
                $cotisation->setUsers(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CoursSuivi[]
     */
    public function getCoursSuivis(): Collection
    {
        return $this->coursSuivis;
    }

    public function addCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if (!$this->coursSuivis->contains($coursSuivi)) {
            $this->coursSuivis[] = $coursSuivi;
            $coursSuivi->setUser($this);
        }

        return $this;
    }

    public function removeCoursSuivi(CoursSuivi $coursSuivi): self
    {
        if ($this->coursSuivis->removeElement($coursSuivi)) {
            // set the owning side to null (unless already changed)
            if ($coursSuivi->getUser() === $this) {
                $coursSuivi->setUser(null);
            }
        }

        return $this;
    }


}
