<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CoursStagiaireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoursStagiaireRepository::class)
 */
#[ApiResource]
class CoursStagiaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apte;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApte(): ?string
    {
        return $this->apte;
    }

    public function setApte(?string $apte): self
    {
        $this->apte = $apte;

        return $this;
    }
}
