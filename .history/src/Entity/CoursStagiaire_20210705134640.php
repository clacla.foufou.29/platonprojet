<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CoursStagiaireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoursStagiaireRepository::class)
 */
#[ApiResource]
class CoursStagiaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $apte;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $present;

    /**
     * ORM\ManyToMany(targetEntity=Cours::class)
     * ORM\JoinColumn(nullable=false)
     */
    private $cours;

    /**
     * ORM\ManyToMany(targetEntity=Stagiaire::class)
     * ORM\JoinColumn(nullable=false)
     */
    private $stagiaires;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApte(): ?bool
    {
        return $this->apte;
    }

    public function setApte(?bool $apte): self
    {
        $this->apte = $apte;

        return $this;
    }

    public function getPresent(): ?bool
    {
        return $this->present;
    }

    public function setPresent(?bool $present): self
    {
        $this->present = $present;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCours()
    {
        return $this->cours;
    }

    /**
     * @param mixed $cours
     */
    public function setCours($cours): void
    {
        $this->cours = $cours;
    }

    /**
     * @return mixed
     */
    public function getStagiaire()
    {
        return $this->stagiaire;
    }

    /**
     * @param mixed $stagiaire
     */
    public function setStagiaire($stagiaire): void
    {
        $this->stagiaire = $stagiaire;
    }

}
