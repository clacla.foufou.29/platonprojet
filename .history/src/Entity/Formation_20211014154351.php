<?php

namespace App\Entity;

use App\Entity\Cours;
use App\Entity\Diplome;
use App\Entity\Categorie;
use App\Entity\Prerequis;
use App\Entity\Prerogative;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\FormationRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=FormationRepository::class)
 */
class Formation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelleFormation;

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbMinStagiaire;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbMaxStagiaire;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbFormateur;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixFormation;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="formations")
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="formation", cascade={"remove"})
     */
    private $cours;

    /**
     * @ORM\ManyToMany(targetEntity=Prerogative::class, mappedBy="formations")
     */
    private $prerogatives;

    /**
     * @ORM\OneToMany(targetEntity=Diplome::class, mappedBy="formation")
     */
    private $diplome;

    /**
     * @ORM\OneToMany(targetEntity=UploadDocumentFormation::class, mappedBy="formation", cascade={"persist", "remove"})
     */
    private $uploadDocumentFormations;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="formation")
     */
    private $user;

    public function __construct()
    {
        $this->cours = new ArrayCollection();
        $this->prerogatives = new ArrayCollection();
        $this->diplome = new ArrayCollection();
        $this->uploadDocumentFormations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleFormation(): ?string
    {
        return $this->libelleFormation;
    }

    public function setLibelleFormation(string $libelleFormation): self
    {
        $this->libelleFormation = $libelleFormation;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getNbMinStagiaire(): ?int
    {
        return $this->nbMinStagiaire;
    }

    public function setNbMinStagiaire(int $nbMinStagiaire): self
    {
        $this->nbMinStagiaire = $nbMinStagiaire;

        return $this;
    }

    public function getNbMaxStagiaire(): ?int
    {
        return $this->nbMaxStagiaire;
    }

    public function setNbMaxStagiaire(int $nbMaxStagiaire): self
    {
        $this->nbMaxStagiaire = $nbMaxStagiaire;

        return $this;
    }

    public function getNbFormateur(): ?int
    {
        return $this->nbFormateur;
    }

    public function setNbFormateur(int $nbFormateur): self
    {
        $this->nbFormateur = $nbFormateur;

        return $this;
    }

    public function getPrixFormation(): ?int
    {
        return $this->prixFormation;
    }

    public function setPrixFormation(int $prixFormation): self
    {
        $this->prixFormation = $prixFormation;

        return $this;
    }


    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): ? Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setFormation($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            // set the owning side to null (unless already changed)
            if ($cour->getFormation() === $this) {
                $cour->setFormation(null);
            }
        }

        return $this;
    }

    public function setCour(Cours $cours): self
    {
        $this->cours=$cours;

        return $this;
    }

    /**
     * @return Collection|Prerogative[]
     */
    public function getPrerogatives(): Collection
    {
        return $this->prerogatives;
    }

    public function addPrerogative(Prerogative $prerogative): self
    {
        if (!$this->prerogatives->contains($prerogative)) {
            $this->prerogatives[] = $prerogative;
            $prerogative->addFormation($this);
        }

        return $this;
    }

    public function removePrerogative(Prerogative $prerogative): self
    {
        if ($this->prerogatives->removeElement($prerogative)) {
            $prerogative->removeFormation($this);
        }

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getLibelleFormation();
    }

    /**
     * @return Collection|Diplome[]
     */
    public function getDiplome(): Collection
    {
        return $this->diplome;
    }

    public function addDiplome(Diplome $diplome): self
    {
        if (!$this->diplome->contains($diplome)) {
            $this->diplome[] = $diplome;
            $diplome->setFormation($this);
        }

        return $this;
    }

    public function removeDiplome(Diplome $diplome): self
    {
        if ($this->diplome->removeElement($diplome)) {
            // set the owning side to null (unless already changed)
            if ($diplome->getFormation() === $this) {
                $diplome->setFormation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UploadDocumentFormation[]
     */
    public function getUploadDocumentFormations(): Collection
    {
        return $this->uploadDocumentFormations;
    }

    public function addUploadDocumentFormation(UploadDocumentFormation $uploadDocumentFormation): self
    {
        if (!$this->uploadDocumentFormations->contains($uploadDocumentFormation)) {
            $this->uploadDocumentFormations[] = $uploadDocumentFormation;
            $uploadDocumentFormation->setFormation($this);
        }

        return $this;
    }

    public function removeUploadDocumentFormation(UploadDocumentFormation $uploadDocumentFormation): self
    {
        if ($this->uploadDocumentFormations->removeElement($uploadDocumentFormation)) {
            // set the owning side to null (unless already changed)
            if ($uploadDocumentFormation->getFormation() === $this) {
                $uploadDocumentFormation->setFormation(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

}
