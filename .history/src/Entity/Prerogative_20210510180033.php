<?php

namespace App\Entity;

use App\Repository\PrerogativeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrerogativeRepository::class)
 */
class Prerogative
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libellePrerogative;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibellePrerogative(): ?string
    {
        return $this->libellePrerogative;
    }

    public function setLibellePrerogative(string $libellePrerogative): self
    {
        $this->libellePrerogative = $libellePrerogative;

        return $this;
    }
}
