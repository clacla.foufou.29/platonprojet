<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CoursRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CoursRepository::class)
 */
class Cours
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomCours;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(
     *      value = "today",
     *      message = "trop tôt"
     * )
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(
     *      value = "today",
     *      message = "trop tôt"
     * )
     * @Assert\Expression(
     *     "this.getDateFin() >= this.getDateDebut()",
     *     message="vacancy.date.not_more_than"
     * )
     */
    private $dateFin;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbStagiaire;

    /**
     * @ORM\Column(type="boolean")
     */
    private $validationCours;

     /**
     * @ORM\ManyToOne(targetEntity=Formation::class, inversedBy="cours")
     */
    private $formation;


    /**
     * @ORM\ManyToOne(targetEntity=Delegation::class, inversedBy="cours")
     */
    private $delegation;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="cours")
     */
    private $formateurs;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="coursCrees")
     */
    private $formateurPrincipal;

    /**
     * @ORM\ManyToOne(targetEntity=Structure::class, inversedBy="cours")
     */
    private $structure;

    /**
     * @ORM\OneToMany(targetEntity=CoursStagiaire::class, mappedBy="cours")
     */
    private $coursStagiaires;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    public function __construct()
    {
        $this->coursStagiaires = new ArrayCollection();
    }

    public function __construct()
    {
        $this->formateurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCours(): ?string
    {
        return $this->nomCours;
    }

    public function setNomCours(string $nomCours): self
    {
        $this->nomCours = $nomCours;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getNbStagiaire(): ?int
    {
        return $this->nbStagiaire;
    }

    public function setNbStagiaire(int $nbStagiaire): self
    {
        $this->nbStagiaire = $nbStagiaire;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getDelegation(): ?Delegation
    {
        return $this->delegation;
    }

    public function setDelegation(?Delegation $delegation): self
    {
        $this->delegation = $delegation;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFormateurs(): Collection
    {
        return $this->formateurs;
    }

    public function addFormateur(User $formateur): self
    {
        if (!$this->formateurs->contains($formateur)) {
            $this->formateurs[] = $formateur;
            $formateur->addCour($this);
        }

        return $this;
    }

    public function removeFormateur(User $formateur): self
    {
        if ($this->formateurs->removeElement($formateur)) {
            $formateur->removeCour($this);
        }

        return $this;
    }

    public function getFormateurPrincipal(): ?User
    {
        return $this->formateurPrincipal;
    }

    public function setFormateurPrincipal(?User $formateurPrincipal): self
    {
        $this->formateurPrincipal = $formateurPrincipal;

        return $this;
    }

    public function getValidationCours(): ?int
    {
        return $this->validationCours;
    }

    public function setValidationCours(int $validationCours): self
    {
        $this->validationCours = $validationCours;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getNomCours();
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}
