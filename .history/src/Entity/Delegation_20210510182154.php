<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\Cours;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\DelegationRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=DelegationRepository::class)
 */
class Delegation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomDelegation;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="delegation")
     */
    private $cours;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="delegation")
     */
    private $formateurs;

    /**
     * @ORM\Column(type="integer")
     */
    private $delegue;

    public function __construct()
    {
        $this->formateurs = new ArrayCollection();
        $this->cours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomDelegation(): ?string
    {
        return $this->nomDelegation;
    }

    public function setNomDelegation(string $nomDelegation): self
    {
        $this->nomDelegation = $nomDelegation;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFormateurs(): Collection
    {
        return $this->formateurs;
    }

    public function addFormateur(User $formateur): self
    {
        if (!$this->formateurs->contains($formateur)) {
            $this->formateurs[] = $formateur;
        }

        return $this;
    }

    public function clearFormateurs(){
        return $this->getFormateurs()->clear();
    }

    public function removeFormateur(User $formateur): self
    {
        if ($this->formateurs->contains($formateur)) {
            $this->formateurs->removeElement($formateur);
        }

        return $this;
    }

    public function getDelegue(): ?int
    {
        return $this->delegue;
    }

    public function setDelegue(int $delegue): self
    {
        $this->delegue = $delegue;

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setDelegation($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            // set the owning side to null (unless already changed)
            if ($cour->getDelegation() === $this) {
                $cour->setDelegation(null);
            }
        }

        return $this;
    }
    /**
     * @return Collection|User[]
     */
    public function __toString(): string
    {
        return (string) $this->getNomDelegation();
    }
}
