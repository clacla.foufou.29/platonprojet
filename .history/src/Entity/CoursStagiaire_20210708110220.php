<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CoursStagiaireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoursStagiaireRepository::class)
 */
#[ApiResource]
class CoursStagiaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $apte;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $present;

    /**
     * @ORM\ManyToOne(targetEntity=Cours::class, inversedBy="coursStagiaire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cours;

    /**
     * @ORM\ManyToOne(targetEntity=Stagiaire::class, inversedBy="coursStagiaire")
     */
    private $stagiaire;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApte(): ?bool
    {
        return $this->apte;
    }

    public function setApte(?bool $apte): self
    {
        $this->apte = $apte;

        return $this;
    }

    public function getPresent(): ?bool
    {
        return $this->present;
    }

    public function setPresent(?bool $present): self
    {
        $this->present = $present;

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    public function getStagiaire(): ?Stagiaire
    {
        return $this->stagiaire;
    }

    public function setStagiaire(?Stagiaire $stagiaire): self
    {
        $this->stagiaire = $stagiaire;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getId();
    }


}
