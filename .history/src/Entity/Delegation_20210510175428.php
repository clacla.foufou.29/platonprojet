<?php

namespace App\Entity;

use App\Repository\DelegationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DelegationRepository::class)
 */
class Delegation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomDelegation;

    /**
     * @ORM\OneToMany(targetEntity=Cours::class, mappedBy="delegation")
     */
    private $cours;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="delegation")
     */
    private $formateurs;

    /**
     * @ORM\Column(type="integer")
     */
    private $delegue;

    public function __construct()
    {
        $this->formateurs = new ArrayCollection();
        $this->cours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomDelegation(): ?string
    {
        return $this->nomDelegation;
    }

    public function setNomDelegation(string $nomDelegation): self
    {
        $this->nomDelegation = $nomDelegation;

        return $this;
    }
}
