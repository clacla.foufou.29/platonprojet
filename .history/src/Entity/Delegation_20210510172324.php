<?php

namespace App\Entity;

use App\Repository\DelegationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DelegationRepository::class)
 */
class Delegation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomDelegation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomDelegation(): ?string
    {
        return $this->nomDelegation;
    }

    public function setNomDelegation(string $nomDelegation): self
    {
        $this->nomDelegation = $nomDelegation;

        return $this;
    }
}
