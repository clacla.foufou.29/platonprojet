<?php

namespace App\Entity;

use App\Entity\Formation;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\PrerogativeRepository;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=PrerogativeRepository::class)
 */
class Prerogative
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libellePrerogative;

    /**
     * @ORM\ManyToMany(targetEntity=Formation::class, inversedBy="prerogatives")
     */
    private $formations;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibellePrerogative(): ?string
    {
        return $this->libellePrerogative;
    }

    public function setLibellePrerogative(string $libellePrerogative): self
    {
        $this->libellePrerogative = $libellePrerogative;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        $this->formations->removeElement($formation);

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getLibellePrerogative();
    }
}
