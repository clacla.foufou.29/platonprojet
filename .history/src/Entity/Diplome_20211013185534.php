<?php

namespace App\Entity;

use App\Entity\User;
use DateTimeInterface;
use App\Entity\Formation;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\DiplomeRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=DiplomeRepository::class)
 * @Vich\Uploadable
 */
class Diplome
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebutFormation;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFinFormation;

    /**
     * @ORM\Column(type="integer")
     */
    private $montantDiplome;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="diplomes")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity=UploadImageDiplome::class, inversedBy="diplome", cascade={"persist", "remove"})
     */
    private $uploadImageDiplome;

    /**
     * @ORM\Column(type="boolean")
     */
    private $accept;

    /**
     * @ORM\ManyToOne(targetEntity=Formation::class, inversedBy="diplome")
     */
    private $formation;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebutFormation(): ?\DateTimeInterface
    {
        return $this->dateDebutFormation;
    }

    public function setDateDebutFormation(?\DateTimeInterface $dateDebutFormation): self
    {
        $this->dateDebutFormation = $dateDebutFormation;

        return $this;
    }

    public function getDateFinFormation(): ?\DateTimeInterface
    {
        return $this->dateFinFormation;
    }

    public function setDateFinFormation(?\DateTimeInterface $dateFinFormation): self
    {
        $this->dateFinFormation = $dateFinFormation;

        return $this;
    }

    public function getMontantDiplome(): ?int
    {
        return $this->montantDiplome;
    }

    public function setMontantDiplome(?int $montantDiplome): self
    {
        $this->montantDiplome = $montantDiplome;

        return $this;
    }


    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getFormation();
    }

    public function getUploadImageDiplome(): ?UploadImageDiplome
    {
        return $this->uploadImageDiplome;
    }

    public function setUploadImageDiplome(?UploadImageDiplome $uploadImageDiplome): self
    {
        $this->uploadImageDiplome = $uploadImageDiplome;

        return $this;
    }

    public function getAccept(): ?bool
    {
        return $this->accept;
    }

    public function setAccept(bool $accept): self
    {
        $this->accept = $accept;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

}
