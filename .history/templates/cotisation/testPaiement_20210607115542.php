<?php 
/**
 * Embbeded Form minimal integration example
 * 
 * To run the example, go to 
 * hhttps://github.com/lyra/rest-php-example
 */

/**
 * I initialize the PHP SDK
 */
require('helpers.php');
require('keys.php')

/* Username, password and endpoint used for server to server web-service calls */
Lyra\Client::setDefaultUsername("20169455");
Lyra\Client::setDefaultPassword("testpassword_L74GN9CAcFg7jZAfJkBGpXUiPkZd0DNajFrdFvn5gvxTl");
Lyra\Client::setDefaultEndpoint("https://api.systempay.fr");

/* publicKey and used by the javascript client */
Lyra\Client::setDefaultPublicKey("20169455:testpublickey_AMY8iM3aIXzS9lIDFf0il5YM5aKviPV107VREOnuZrgsD");

/* SHA256 key */
Lyra\Client::setDefaultSHA256Key("HY3Oa1EzWMDDldaIqufUqHb3eBvVcKlO2Y29s2HKrKNrp");
/** 
 * Initialize the SDK 
 * see keys.php
 */
$client = new Lyra\Client();

if (isset($_GET['requestObject'])) {
  $store = json_decode($_GET['requestObject']);
} else {
  $store = array( 
    "amount" => 250,
    "currency" => "EUR", 
    "orderId" => uniqid("MyOrderId"),
    "customer" => array(
      "email" => "sample@example.com"
    )
  );
}

/**
* I create a formToken
*/

$response = $client->post("V4/Charge/CreatePayment", $store);

//* I check if there are some errors */
if ($response['status'] != 'SUCCESS') {
  /* an error occurs */
  $error = $response['answer'];
  header("Content-Type", "application/json");
  header('HTTP/1.1 500 Internal Server Error');
  echo '{"error": "' . $error['errorCode'] . '", "_type": "DemoError" }';
  die();
}

/* everything is fine, I extract the formToken */
$formToken = $response["answer"]["formToken"];
header("Content-Type", "application/json");
echo '{"formToken": "' . $formToken . '"", "_type": "DemoFormToken" }';

/**
 * I create a formToken
 */
$store = array("amount" => 250,
"currency" => "EUR", 
"orderId" => uniqid("MyOrderId"),
"customer" => array(
  "email" => "sample@example.com"
));
dd($store);
$response = $client->post("V4/Charge/CreatePayment", $store);

/* I check if there are some errors */
if ($response['status'] != 'SUCCESS') {
    /* an error occurs, I throw an exception */
    display_error($response);
    $error = $response['answer'];
    throw new Exception("error " . $error['errorCode'] . ": " . $error['errorMessage'] );
}

/* everything is fine, I extract the formToken */
$formToken = $response["answer"]["formToken"];

?>
<!DOCTYPE html>
<html>

  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Javascript library. Should be loaded in head section -->
    <script src="https://krypton.purebilling.io/static/js/krypton-client/dev/stable/kr-payment-form.min.js" kr-public-key="<?php echo $client->getPublicKey();?>" kr-post-url-success="paid.php">
    </script>

    <!-- theme and plugins. should be loaded after the javascript library -->
    <!-- not mandatory but helps to have a nice payment form out of the box -->
    <link rel="stylesheet" href="https://krypton.purebilling.io/static/js/krypton-client/dev/ext/classic-reset.css">
    <script src="https://krypton.purebilling.io/static/js/krypton-client/dev/ext/classic.js">
    </script>
  </head>

  <body>
    <!-- payment form -->
    <div class="kr-embedded" kr-form-token="<?php echo $formToken;?>">

      <!-- payment form fields -->
      <div class="kr-pan"></div>
      <div class="kr-expiry"></div>
      <div class="kr-security-code"></div>

      <!-- payment form submit button -->
      <button class="kr-payment-button"></button>

      <!-- error zone -->
      <div class="kr-form-error"></div>
    </div>
  </body>

</html>