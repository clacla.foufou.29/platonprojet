
<?php
/**
 * Get the client
 */
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Define configuration
 */

/* Username, password and endpoint used for server to server web-service calls */
Lyra\Client::setDefaultUsername("201694455");
Lyra\Client::setDefaultPassword("testpassword_L74GN9CAcFg7jZAfJkBGpXUiPkZd0DNajFrdFvn5gvxTl");
Lyra\Client::setDefaultEndpoint("https://api.systempay.fr");

/* publicKey and used by the javascript client */
Lyra\Client::setDefaultPublicKey("20169455:testpublickey_AMY8iM3aIXzS9lIDFf0il5YM5aKviPV107VREOnuZrgsD");

/* SHA256 key */
Lyra\Client::setDefaultSHA256Key("38453613e7f44dc58732bad3dca2bca3");